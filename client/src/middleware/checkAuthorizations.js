import axios from 'axios';

const baseURL = process.env.NODE_ENV === "production" ? `${process.env.REACT_APP_API_URL}` : `${process.env.REACT_APP_API_URL_DEV}`;

const checkAuthorizations = store => next => action => {

    // Add a request interceptor
    // Adding on each request the Authorization Bearer + token if exists
    axios.interceptors.request.use(
        config => {
            const token = localStorage.getItem('authToken');
            config.baseURL = baseURL;
            if (token) {
                config.headers['Authorization'] = 'Bearer ' + token;
            }
            return config;
        },
        error => Promise.reject(error)
    );

    // Add a response interceptor

    axios.interceptors.response.use((response) => {
        return response
    }, function (error) {
        const originalRequest = error.config;

        if (error.response.status === 401 && originalRequest.url === `${baseURL}/auth/refresh-token`) {
            localStorage.removeItem('authToken');
            localStorage.removeItem('refreshToken');
            localStorage.removeItem('user');
            window.history.go('/login');
            return Promise.reject(error);
        }

        if (error.response.status === 401 && !originalRequest._retry) {

            originalRequest._retry = true;
            const refreshToken = localStorage.getItem('refreshToken');
            return axios.post(`${baseURL}/auth/refresh-token`, { token : refreshToken })
                .then(res => {
                    if (res.status === 201) {
                        localStorage.setItem('authToken',res.data.authToken);
                        localStorage.setItem('refreshToken',res.data.refreshToken);
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('authToken');
                        return axios(originalRequest);
                    }
                })
        }
        return Promise.reject(error);
    });

    next(action);

};

export default checkAuthorizations;