export default {
    apiUrl: process.env.NODE_ENV !== 'production' ? 'http://localhost:3001/api/v1' : process.env.REACT_APP_API_URL,
    proxyUrl: process.env.NODE_ENV !== 'production' ? '' : process.env.REACT_APP_CORS_PROXY,
}