import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index';
import App from './App';
import './App.scss';
import * as serviceWorker from './serviceWorker';
import * as types from './actions/actionTypes';
import checkAuthorizations from './middleware/checkAuthorizations';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    .empty-list{
        margin: 2rem;
        padding: 5rem 0;
        display: flex;
        align-items: center;
        justify-content: center;
        color: var(--text-color-secondary)
    }
`

const store = createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk, checkAuthorizations),
));

const authToken = localStorage.getItem('authToken');

if (authToken) {
    store.dispatch({ type: types.USER_REQUEST });
}

Modal.setAppElement('#root');

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
        <GlobalStyle />
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();