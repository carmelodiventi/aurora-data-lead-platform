import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { useToasts } from 'react-toast-notifications';
import styled from 'styled-components';
import Main from '../../components/Main';
import Toolbar from '../../components/Toolbar';
import ContentWrapper from '../../components/ContentWrapper';
import UserForm from '../../components/user/UserForm'
import Block from '../../components/Block';
import { IoIosPerson } from 'react-icons/io';
import { addUser, fetchUser } from '../../services/users.service';
import { fetchVendors } from '../../services/vendor.service';

const Grid = styled.div`
    padding: 2rem;
    display: grid;
    grid-auto-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
`;

const GridItem = styled.div``;

const AddUser = ({ addUser, fetchVendors, history, vendors }) => {

    const { addToast } = useToasts();

    useEffect(() => {
        fetchVendors()
    }, [])

    const handleSubmit = (values) => {
        addUser(values).then(res => {
            if (res.success) {
                addToast(res.message, {
                    appearance: 'success',
                    autoDismiss: true
                }, () => history.goBack())
            }
            else if (!res.success) {
                addToast(res.error, {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        })
    }

    return (
        <Main>
            <Toolbar title="Add a new User" goBack={() => history.goBack()} />
            <ContentWrapper>
                <Grid>
                    <GridItem>
                        <Block
                            title="User Informations"
                            icon={<IoIosPerson />}
                            description="Add the User informations and the role that will cover on Aurora, can you assign also the Vendor that belong to">
                            <UserForm onSubmit={handleSubmit} initialValues={{ is_active: true }} vendors={vendors.list} />
                        </Block>
                    </GridItem>
                </Grid>
            </ContentWrapper>
        </Main>
    );
};



function mapDispatchToProps(dispatch) {
    return bindActionCreators({ addUser, fetchVendors }, dispatch)
}

function mapStateToProps(state) {
    return {
        vendors: state.vendors
    }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(AddUser);