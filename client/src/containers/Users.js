import React, { Component } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import ContentWrapper from '../components/ContentWrapper';
import ListItem from '../components/ListItem';
import Loading from '../components/Loading';
import NoItemsFound from '../components/NoItemsFound';
import { Button } from '../components/form';
import styled from 'styled-components';
import { fetchUsers, deleteUser } from '../services/users.service';


const UsersList = styled.div`
    padding: 2rem;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 2rem;
`;

class Users extends Component {

    componentDidMount() {
        this.props.fetchUsers()
    }

    handleDeleteUser = id => {
        this.props.deleteUser(id)
    }

    handleDetailsUser = id => {
        this.props.history.push(`users/edit/${id}`)
    }

    render() {
        const { history, users: { list, isDispatching } } = this.props;
        return (
            <Main>
                <Toolbar title="Users">
                    <Button variant="contained" onClick={() => history.push('/users/add')}>
                        Add User
                    </Button>
                </Toolbar>
                <ContentWrapper>
                    <UsersList>
                        {list.map(({ _id, firstname, lastname, email, role }) => <ListItem name={`${firstname && lastname ? firstname + ' ' + lastname : email}`} description={role} id={_id} key={_id} onDelete={this.handleDeleteUser} onClick={this.handleDetailsUser} />)}
                    </UsersList>
                    <Loading showing={isDispatching} />
                    {list.length === 0 && <NoItemsFound title="No items found" text="No users found, add a User" />}
                </ContentWrapper>
            </Main>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.users
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchUsers, deleteUser }, dispatch)
}


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withRouter
)(Users);