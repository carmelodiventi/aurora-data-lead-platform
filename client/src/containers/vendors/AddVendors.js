import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { useToasts } from 'react-toast-notifications';
import styled from 'styled-components';
import Main from '../../components/Main';
import Toolbar from '../../components/Toolbar';
import ContentWrapper from '../../components/ContentWrapper';
import VendorsForm from '../../components/vendors/VendorForm'
import Block from '../../components/Block';
import { IoIosArchive } from 'react-icons/io';
import { addVendor } from '../../services/vendor.service';


const Grid = styled.div`
    padding: 2rem;
    display: grid;
    grid-auto-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
`;

const GridItem = styled.div``;

const AddVendors = ({ addVendor, history }) => {

    const { addToast } = useToasts();

    const handleSubmnit = (values) => {
        addVendor(values).then(res => {
            if (res.success) {
                addToast('Vendor added succesfully', {
                    appearance: 'success',
                    autoDismiss: true
                }, () => history.goBack())
            }
            else if (!res.success) {
                addToast(res.error, {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        })
    }

    return (
        <Main>
            <Toolbar title="Add a new Vendor" goBack={ () => history.push('/vendors') } />
            <ContentWrapper>
                <Grid>
                    <GridItem>
                        <Block
                            title="Vendor Informations"
                            icon={<IoIosArchive />}
                            description="Insert all informations, such name, website, and the company logo"
                        >
                            <VendorsForm onSubmit={handleSubmnit} />
                        </Block>
                    </GridItem>
                </Grid>
            </ContentWrapper>
        </Main>
    );
};


function mapStateToProps(state) {
    return {
        vendors: state.vendors
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ addVendor }, dispatch)
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(AddVendors);