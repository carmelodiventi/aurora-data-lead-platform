import React from 'react';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';

const Dashboard = () => {
    return (
        <Main>
            <Toolbar title="Dashboard"/>
        </Main>
    );
};

export default Dashboard;