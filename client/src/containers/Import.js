import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import CSVReader from 'react-csv-reader'
import { IoIosRefresh } from 'react-icons/io';

import List from '../components/csv/List';
import Main from '../components/Main';
import TemplateList from '../components/contacts/Import/TemplateList';
import { Button, IconButton } from '../components/form';
import Toolbar from '../components/Toolbar';
import ContentWrapper from '../components/ContentWrapper';
import Dialog from '../components/Dialog';
import ListForm from '../components/list/ListForm';
import Loading from '../components/Loading';

import { fieldsMapperList } from '../constants/fieldmapper';
import { fortmatCSV } from '../helpers/helper';
import { saveList } from '../services/lists.service';
import { handleContactsImported, saveContacts } from '../services/contacts.service';
import { handleMapFields, handleDeleteMapFields, fetchTemplates, handleResetMapFields } from '../services/import.service';
import { fetchVendors } from '../services/vendor.service'
import { showDialog, hideDialog } from '../services/dialog.service';




class Import extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            filename: '',
            fieldMapTemplate: '',
            fieldsMapTemplateItem: [],
            fieldsMapped: false,
            fileProvided: false,
            toggleListForm: false
        }
    }

    componentDidMount() {
        this.props.fetchTemplates();
        this.props.fetchVendors();
    }


    componentDidUpdate(prevProps) {
        
        if (prevProps.import.items !== this.props.import.items) {
            this.setState({
                rows: this.props.import.items
            })
        }
    }

    handleFiles = (rows, filename) => {

        const { toastManager } = this.props;

        this.setState({
            filename,
            fileProvided: true
        });

        this.props.handleContactsImported(rows).then(() => {

            toastManager.add('File Imported, Now map the Columns Field', {
                appearance: 'info',
                autoDismiss: true
            })

        });

    }

    handleError = () => {
        this.props.handleContactsImported([])
    }

    handleRefresh = () => {
        this.setState({
            rows: [],
            filename: '',
            fieldsMappedList: [],
            fieldMapTemplate: null,
            fieldsMapTemplateItem: [],
            fieldsMapped: false,
            fileProvided: false,
            toggleListForm: false
        });
        this.props.handleContactsImported([]);
        this.props.handleDeleteMapFields();
    }


    handleToggleForm = () => {
        this.setState({
            toggleListForm: !this.state.toggleListForm
        })
    }

    handleSaveList = ({ listName: name, vendor }) => {

        const { fieldsMapped } = this.props.import;
        const { toastManager } = this.props;
        let { rows, fieldMapTemplate: template } = this.state;
        let itemslist = fortmatCSV(rows, fieldsMapped);

        this.props.saveList(name, itemslist, template, vendor).then(res => {

            if (res.success) {

                toastManager.add('List created successfully', {
                    appearance: 'success',
                    autoDismiss: true
                })

                this.handleRefresh()

            }
            else if (!res.success) {
                toastManager.add(res.error, {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        });
    }

    handleMapFields = (item) => {

        this.setState({
            fieldsMapped: true
        },
            this.props.handleMapFields(item)
        );


    }

    handleChangeTemplate = ({ value }) => {

        let template;

        if (value !== '') {
            const { list } = this.props.templates;
            template = list.find(item => item._id === value).fields;
        }

        let fields = template || fieldsMapperList;

        this.setState({
            fieldMapTemplate: value,
            fieldsMapTemplateItem: fields
        })

    }

    render() {

        const { fieldsMapped, fieldMapTemplate, fieldsMapTemplateItem, rows, fileProvided, filename, toggleListForm } = this.state;
        const { history } = this.props;

        return (
            <Main>

                <Toolbar title="Import a list of Contacts">

                    {!fileProvided &&
                        <>
                            <Button variant="contained" onClick={() => history.push('/templates')}>
                                Create Template
                            </Button>

                            <CSVReader
                                cssClass="csv-reader-button"
                                label="Import contacts"
                                onFileLoaded={this.handleFiles}
                                onError={this.handleError}
                                inputId="csv"
                                inputStyle={{ display: 'none' }}
                                parserOptions={{ header: false }}
                            />

                        </>
                    }


                    {fileProvided &&
                        <>

                            <TemplateList
                                fieldMapTemplate={fieldMapTemplate}
                                handleChangeTemplate={this.handleChangeTemplate}
                                list={this.props.templates.list}
                            />

                            <Button disabled={!fieldsMapped} onClick={this.handleToggleForm}>
                                Save as List
                            </Button>

                        </>
                    }


                    <IconButton onClick={this.handleRefresh}>
                        <IoIosRefresh />
                    </IconButton>


                </Toolbar>

                <ContentWrapper>
                    <List
                        handleMapFields={this.handleMapFields}
                        fieldsMapped={this.props.import.fieldsMapped}
                        fieldsMapTemplateItem={fieldsMapTemplateItem}
                        fieldMapTemplate={fieldMapTemplate}
                        rows={rows}
                    />
                </ContentWrapper>


                <Dialog
                    title={filename}
                    description={`To save a collection lists, type the name ${this.state.filename} to confirm.`}
                    open={toggleListForm}
                    onCancel={this.handleToggleForm}
                    loading={this.props.import.isDispatching}
                    success={this.props.import.success}
                    error={this.props.import.error}>

                    <ListForm
                        listName={this.state.filename}
                        vendors={this.props.vendors.list}
                        handleCancel={this.handleToggleForm}
                        onSubmit={values => this.handleSaveList(values)}
                    />

                </Dialog>

                <Loading showing={this.props.import.isDispatching} />

            </Main>
        )
    }
}


function mapStateToProps(state) {
    return {
        import: state.import,
        templates: state.templates,
        vendors: state.vendors
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ handleContactsImported, saveList, saveContacts, handleMapFields, handleDeleteMapFields, fetchTemplates, handleResetMapFields, showDialog, hideDialog, fetchVendors }, dispatch)
}

export default compose(
    withToastManager,
    connect(mapStateToProps, mapDispatchToProps)
)(Import);