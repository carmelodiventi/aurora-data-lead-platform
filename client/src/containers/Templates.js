import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import styled from 'styled-components';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import { Button } from '../components/form';
import ContentWrapper from '../components/ContentWrapper';
import NoItemsFound from '../components/NoItemsFound';
import Loading from '../components/Loading';
import ListItem from '../components/ListItem';
import { fetchTemplates, createTemplate, deleteTemplate } from '../services/import.service';


const TemplateList = styled.div`
    padding: 2rem;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 2rem;
`;


class Templates extends Component {

    componentDidMount() {
        this.props.fetchTemplates();
    }

    handleDeleteTemplate = id => {
        this.props.deleteTemplate(id);
    }

    handleDetailsTemplate = id => {
        this.props.history.push(`templates/edit/${id}`)
    }

    render() {

        const { templates: { list, isDispatching, success, error }, history } = this.props;

        return (
            <Main>
                <Toolbar title="Templates List">
                    <Button variant="contained" onClick={() => history.push('/templates/add')}>
                        Add Template
                    </Button>
                </Toolbar>
                <ContentWrapper>
                    <TemplateList>
                        {list.map(({ _id, name }) => <ListItem name={name} id={_id} key={_id} onDelete={this.handleDeleteTemplate} onClick={this.handleDetailsTemplate} />)}
                    </TemplateList>
                    {list.length === 0 && <NoItemsFound title="No items found" text="Add a Template" />}
                    {isDispatching && <Loading />}
                </ContentWrapper>
            </Main>
        );
    }
}

function mapStateToProps(state) {
    return {
        templates: state.templates
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchTemplates, createTemplate, deleteTemplate }, dispatch)
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(Templates);