import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import { fetchLists, fetchListByName, fetchListByDates, deleteList, resetListState } from '../services/lists.service';
import { resetContacts } from '../services/contacts.service';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import ContentWrapper from '../components/ContentWrapper';
import List from '../components/list/List';
import Loading from '../components/Loading';
import Pagination from 'react-paginate';
import { IoIosArrowForward, IoIosArrowBack, IoIosRefresh } from 'react-icons/io';
import { IconButton } from '../components/form';
import SearchBox from '../components/SearchBox';
import SearchByDates from '../components/SearchByDates';



class Lists extends Component {

  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      limit: 100
    }
  }

  componentDidMount() {
    this.fetchLists();
  }

  componentWillUnmount() {
    this.props.resetContacts();
  }


  fetchLists = () => {
    const { offset, limit } = this.state;
    this.props.fetchLists(offset, limit);
  }

  handleDeleteList = id => {
    const { toastManager } = this.props;
    this.props.deleteList(id).then(res => {
      if (res.success) {
        toastManager.add('List deleted successfully', {
          appearance: 'success',
          autoDismiss: true
        })
      }
      if (res.error) {
        toastManager.add(res.error, {
          appearance: 'error',
          autoDismiss: true
        })
      }
    });
  }

  handleSelectItem = (id) => {
    this.props.history.push(`/lists/${id}`);
  }

  handleSearchList = name => {
    const { offset, limit } = this.state;
    this.props.fetchListByName(name, offset, limit);
  }

  handleSearchListByDate = (startDate, endDate) => {
    const { offset, limit } = this.state;
    this.props.fetchListByDates(startDate, endDate, offset, limit);
  }

  handleRefresh = () => {
    const { offset, limit } = this.state;
    this.props.fetchLists(offset, limit);
  }

  handlePageChange = ({ selected }) => {
    const { limit } = this.state;
    this.setState({
      offset: selected * limit
    }, () => {
      this.fetchLists()
    })

  }


  render() {

    const { offset, limit } = this.state;
    const { lists } = this.props;

    return (
      <Main>

        <Toolbar title="Lists">
          <SearchByDates onChange={this.handleSearchListByDate} onSubmit={this.handleFilterListByDate} />
          <SearchBox onSubmit={this.handleSearchList} placeholder="Search by Name" />
          <IconButton onClick={this.handleRefresh}>
            <IoIosRefresh />
          </IconButton>
        </Toolbar>

        <ContentWrapper>

          <List
            handleDeleteList={this.handleDeleteList}
            handleGetLists={this.handleGetLists}
            handleSelectItem={this.handleSelectItem}
            rows={lists.items}
            offset={offset}
            limit={limit}
            count={lists.totalCount}
            handlePageChange={this.handlePageChange}
          />

          {lists.totalCount > limit &&
            <Pagination
              containerClassName="pagination"
              previousLabel={<IoIosArrowBack />}
              nextLabel={<IoIosArrowForward />}
              onPageChange={this.handlePageChange}
              pageCount={lists.totalCount / limit}
            />
          }
        </ContentWrapper>

        <Loading showing={lists.isDispatching} />

      </Main>
    )

  }
}


function mapStateToProps(state) {
  return {
    lists: state.lists
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchLists, fetchListByName, fetchListByDates, deleteList, resetListState, resetContacts }, dispatch)
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
  withToastManager
)(Lists);