import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useToasts } from 'react-toast-notifications'
import { forgotPassword, checkCode, resetPassword } from '../../services/authentication.service';
import logo from '../../assets/img/aurora.png';
import Loading from '../../components/Loading';
import ForgotPasswordForm from '../../components/auth/ForgotPasswordForm';
import CheckCodeForm from '../../components/auth/CheckCodeForm';
import ResetPasswordForm from '../../components/auth/ResetPasswordForm';


const ForgotPassword = ({ auth: { isDispatching, error, success, hasCode, codeIsValid }, forgotPassword, resetPassword, checkCode, history }) => {

    const [state, setState] = useState({
        code: null,
        email: null
    });

    const toast = useToasts();

    const onForgotPassword = values => {

        forgotPassword(values).then((res) => {
            if (res.success) {
                toast.addToast(res.message, {
                    appearance: "info",
                    autoDismiss: true
                }, () => {
                    setState({
                        ...state,
                        email: values.email
                    })
                })
            }
            else if (!res.success) {
                toast.addToast(res.error, {
                    appearance: "error",
                    autoDismiss: true
                })
            }

        });

    }

    const onCheckCode = (values) => {

        const newValues = { ...state, ...values }; // merging state with values

        checkCode(newValues).then(res => {
            if (res.success) {
                toast.addToast(res.message, {
                    appearance: "info",
                    autoDismiss: true
                }, () => {
                    setState({
                        ...state,
                        code: values.code
                    })
                })
            }
            else if (!res.success) {
                toast.addToast(res.error, {
                    appearance: "error",
                    autoDismiss: true
                })
            }
        })
    }

    const onResetPassword = (values) => {
        const newValues = { ...state, ...values }; // merging state with values
        resetPassword(newValues).then((res) => {
            if (res.success) {
                toast.addToast(res.message, {
                    appearance: "info",
                    autoDismiss: true
                }, () => {
                    history.push('/login');
                })
            }
            else if (!res.success) {
                toast.addToast(res.error, {
                    appearance: "error",
                    autoDismiss: true
                })
            }
        })
    }

    return (
        <div className="auth">

            <div className="auth--image" style={{
                backgroundImage: 'url(https://source.unsplash.com/random)',
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
            }} />

            <div className="auth-container">
                <div className="auth--component">
                    <img className="logo" src={logo} />
                    {!hasCode && <ForgotPasswordForm onSubmit={onForgotPassword} />}
                    {hasCode && !codeIsValid && <CheckCodeForm onSubmit={onCheckCode} />}
                    {codeIsValid && <ResetPasswordForm onSubmit={onResetPassword} />}
                </div>
                <Loading showing={isDispatching} />
            </div>

        </div>
    )
}

const mapStateToProps = state => ({ auth: state.auth })
const mapDispatchToProps = dispatch => (bindActionCreators({ forgotPassword, checkCode, resetPassword }, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)