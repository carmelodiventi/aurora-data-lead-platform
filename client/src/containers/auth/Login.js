import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useToasts } from 'react-toast-notifications';
import { load } from 'recaptcha-v3';
import { authenticate } from '../../services/authentication.service';
import logo from '../../assets/img/aurora.png';
import Loading from '../../components/Loading';
import LoginForm from '../../components/auth/LoginForm';

const Login = props => {

    const { isDispatching } = props.auth;
    const toast = useToasts();

    useEffect(() => {
        if (props.auth.isLogged) { }
    }, [props.auth.isLogged])

    const onAuthenticate = async (values) => {
        const recaptcha = await load(process.env.REACT_APP_KEY_RECAPTCHA_PUBLIC);
        const g_token = await recaptcha.execute('login');
        const data = { ...values, g_token};

        props.authenticate(data).then(res => {
            if (res.success) {
                props.history.push('/')
            }
            else if(!res.success){
                toast.addToast(res.error,{
                    appearance: "error",
                    autoDismiss: true
                })
            }
        })
    }

    return (
        <div className="auth">


            <div className="auth--image" style={{
                backgroundImage: 'url(https://source.unsplash.com/random)',
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
            }} />

            <div className="auth-container">
                <div className="auth--component">
                    <img className="logo" src={logo} />
                    <LoginForm onSubmit={onAuthenticate} />
                </div>
                <Loading showing={isDispatching} />
            </div>

        </div>
    )
}

const mapStateToProps = state => ({ auth: state.auth})

const mapDispatchToProps = (dispatch) => bindActionCreators({ authenticate }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Login)