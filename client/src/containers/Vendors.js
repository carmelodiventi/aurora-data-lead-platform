import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import ContentWrapper from '../components/ContentWrapper';
import NoItemsFound from '../components/NoItemsFound';
import ListItem from '../components/ListItem';
import styled from 'styled-components';
import Loading from '../components/Loading';
import { Button } from '../components/form';
import { fetchVendors, deleteVendor } from '../services/vendor.service';

const VendorsList = styled.div`
    padding: 2rem;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 2rem;
`;

class Vendors extends Component {

    componentDidMount() {
        this.props.fetchVendors()
    }

    handleDeleteVendor = id => {
        this.props.deleteVendor(id)
    }

    handleDetailsVendor = id => {
        this.props.history.push(`vendors/edit/${id}`)
    }

    render() {

        const { history, vendors: { list, isDispatching } } = this.props;

        return (
            <Main>
                <Toolbar title="Vendors List">
                    <Button variant="contained" onClick={() => history.push('/vendors/add')}>
                        Add Vendor
                    </Button>
                </Toolbar>
                <ContentWrapper>
                    <VendorsList>
                        {list.map(({ _id, name, logo }) => <ListItem name={name} id={_id} image={logo} key={_id} onDelete={this.handleDeleteVendor} onClick={this.handleDetailsVendor} />)}
                    </VendorsList>
                    {list.length === 0 && <NoItemsFound title="No items found" text="No vendors found, add a Vendor" />}
                    {isDispatching && <Loading />}
                </ContentWrapper>
            </Main>
        );
    }

}

function mapStateToProps(state) {
    return {
        vendors: state.vendors
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchVendors, deleteVendor }, dispatch)
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(Vendors);