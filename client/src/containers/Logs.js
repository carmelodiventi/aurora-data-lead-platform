import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { fetchLogs } from '../services/logs.service';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import ContentWrapper from '../components/ContentWrapper';
import List from '../components/logs/List';
import Loading from '../components/Loading';
import Pagination from 'react-paginate';
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io';


class Logs extends Component {

  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      limit: 100
    }
  }

  componentDidMount() {
    this.fetchLogs();
  }


  fetchLogs = () => {
    const { offset, limit } = this.state;
    this.props.fetchLogs(offset, limit);
  }

  
  handlePageChange = ({ selected }) => {
    const { limit } = this.state;
    this.setState({
      offset: selected * limit
    }, () => {
      this.fetchLogs()
    })

  }

  render() {

    const { offset, limit } = this.state;
    const { logs } = this.props;

    return (
      <Main>

        <Toolbar title="Logs" />

        <ContentWrapper>

          <List
            rows={logs.items}
            offset={offset}
            limit={limit}
            count={logs.totalCount}
            handlePageChange={this.handlePageChange}
          />

          {logs.totalCount > limit &&
            <Pagination
              containerClassName="pagination"
              previousLabel={<IoIosArrowBack />}
              nextLabel={<IoIosArrowForward />}
              onPageChange={this.handlePageChange}
              pageCount={logs.totalCount / limit}
            />
          }
        </ContentWrapper>

        <Loading showing={logs.isDispatching} />

      </Main>
    )

  }
}


function mapStateToProps(state) {
  return {
    logs: state.logs
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchLogs }, dispatch)
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(Logs);