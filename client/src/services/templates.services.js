import * as types from '../actions/actionTypes';
import axios from 'axios';


export const fetchTemplates = () => async dispatch => {
    try {
        dispatch({ type: types.REQUEST_TEMPLATES });
        const url = `/templates`;
        const res = await axios.get(url);
        dispatch({ type: types.GET_TEMPLATES, payload: res.data });
    } catch (error) {
        dispatch({ type: types.GET_TEMPLATES_FAILS, payload: error.response.data })
    }
}

export const fetchTemplate = id => async dispatch => {
    try {
        dispatch({ type: types.REQUEST_TEMPLATES });
        const url = `/templates/${id}`;
        const res = await axios.get(url);
        dispatch({ type: types.GET_TEMPLATE, payload: res.data });
    } catch (error) {
        dispatch({ type: types.GET_TEMPLATE_FAILS, payload: error.response.data })
    }
}


export const createTemplate = ({ templateName: name , templateFields: fields  }) => async dispatch => {
    try {
        dispatch({ type: types.REQUEST_TEMPLATES });
        const url = `/templates`;
        const res = await axios.post(url, { name, fields });
        dispatch({ type: types.SAVE_TEMPLATE, payload: res.data });
        return res.data;
    } catch (error) {
        dispatch({ type: types.SAVE_TEMPLATE_FAILS, payload: error.response.data })
        return error.response.data;
    }
}

export const updateTemplate = ({ id, templateName: name , templateFields: fields  }) => async dispatch => {
    try {
        dispatch({ type: types.REQUEST_TEMPLATES });
        const url = `/templates/${id}`;
        const res = await axios.patch(url, { name, fields });
        dispatch({ type: types.SAVE_TEMPLATE, payload: res.data });
        return res.data;
    } catch (error) {
        dispatch({ type: types.SAVE_TEMPLATE_FAILS, payload: error.response.data })
        return error.response.data;
    }
}


export const deleteTemplate = id => async dispatch => {
    try {
        dispatch({ type: types.REQUEST_TEMPLATES });
        const url = `/templates/${id}`;
        const res = await axios.delete(url);
        dispatch({ type: types.DELETE_TEMPLATE, payload: res.data });
    } catch (error) {
        dispatch({ type: types.DELETE_TEMPLATE_FAILS, payload: error.response.data })
    }
}