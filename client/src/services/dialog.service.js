import * as types from '../actions/actionTypes';

export const showDialog = modalProps => dispatch => {
    dispatch({
        type: types.SHOW_MODAL,
        payload : { modalProps }
    })
}

export const hideDialog = () => dispatch => {
    dispatch({type: types.HIDE_MODAL})
}