import * as types from '../actions/actionTypes';
import axios from 'axios';


export const fetchUsers = () => async dispatch => {

    try {
        dispatch({ type: types.REQUEST_USERS })
        const url = `/users`;
        const res = await axios.get(url);
        dispatch({ type: types.GET_USERS, payload: res.data })
    } catch (error) {
        dispatch({ type: types.GET_USERS_FAILS, payload: error.response.data })
    }

}

export const fetchUser = id => async dispatch => {

    try {
        dispatch({ type: types.REQUEST_USERS })
        const url = `/users/${id}`;
        const res = await axios.get(url);
        dispatch({ type: types.GET_USER, payload: res.data })
    } catch (error) {
        dispatch({ type: types.GET_USER_FAILS, payload: error.response.data })
    }

}

export const addUser = data => async dispatch => {

    try {
        dispatch({ type: types.REQUEST_USERS })
        const url = `/users`;
        const res = await axios.post(url, {...data});
        dispatch({ type: types.ADD_USER, payload: res.data })
        return res.data;
    } catch (error) {
        dispatch({ type: types.ADD_USER_FAILS, payload: error.response.data })
        return error.response.data;
    }

}

export const updateUser = (id, values) => async dispatch => {

    try {
        dispatch({ type: types.REQUEST_USERS })
        const url = `/users/${id}`;
        const res = await axios.patch(url, { id, ...values });
        dispatch({ type: types.GET_USER, payload: res.data })
        return res.data;
    } catch (error) {
        dispatch({ type: types.GET_USER_FAILS, payload: error.response.data })
        return error.response.data;
    }

}

export const deleteUser = id => async dispatch => {

    try {
        dispatch({ type: types.REQUEST_USERS})
        const url = `/users/${id}`;
        const res = await axios.delete(url);
        dispatch({ type: types.DELETE_USER, payload: res.data })
    } catch (error) {
        dispatch({ type: types.DELETE_USER_FAILS, payload: error.response.data })
    }

}