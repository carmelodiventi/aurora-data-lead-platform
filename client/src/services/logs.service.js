import * as types from '../actions/actionTypes';
import axios from 'axios';


export const fetchLogs = (offset, limit) => async dispatch => {

    try {
        dispatch({ type: types.REQUEST_ITEMS });
        const url = `/logs?offset=${offset}&limit=${limit}`;
        const res = await axios.get(url);
        dispatch({type: types.GET_LOGS,payload: res.data})
    } catch (error) {
        dispatch({ type: types.GET_LOGS_FAILS, payload: error.response.data })
    
    }

}
