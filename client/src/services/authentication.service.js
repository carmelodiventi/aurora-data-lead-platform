import * as types from '../actions/actionTypes';
import axios from 'axios';

export const authenticate = ({ email, password, g_token }) => async dispatch => {

    try {

        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.post(`/auth/login`, { email, password, g_token });
        dispatch({ type: types.LOGIN, payload: res.data });
        localStorage.setItem('authToken', res.data.authToken);
        localStorage.setItem('refreshToken', res.data.refreshToken);
        localStorage.setItem('user', JSON.stringify(res.data.user));
        return res.data;
    }
    catch (error) {
        dispatch({ type: types.LOGIN_FAILS, payload: error.response.data })
        return error.response.data
    }

}


export const forgotPassword = ({email}) => async dispatch => {

    try {
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.post(`/auth/forgot-password`, {email});
        dispatch({ type: types.FORGOT_PASSWORD, payload: res.data });
        return res.data;
    }
    catch (error) {
        dispatch({ type: types.FORGOT_PASSWORD_FAILS, payload: error.response.data })
        return error.response.data
    }

}

export const checkCode = ({email, code}) => async dispatch => {
    try{
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.post(`/auth/check-code`, {email, code});
        dispatch({ type: types.CHECK_CODE, payload: res.data });
        return res.data;
    }
    catch(error){
        dispatch({ type: types.CHECK_CODE_FAILS, payload: error.response.data })
        return error.response.data;
    }
}

export const resetPassword = ({email, code, password}) => async dispatch => {
    try{
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.post(`/auth/reset-password`, { email, code, password });
        dispatch({ type: types.RESET_PASSWORD, payload: res.data });
        return res.data;
    }
    catch(error){
        dispatch({ type: types.RESET_PASSWORD_FAILS, payload: error.response.data })
        return error.response.data;
    }
}

export const unAuthenticate = () => async dispatch => {

    try{
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.get(`/auth/logout`);
        dispatch({ type: types.LOGOUT, payload: res.data });
        return res.data;
    }
    catch(error){
        dispatch({ type: types.LOGOUT_FAILS, payload: error.response.data })
        return error.response.data;
    }

   
}

export const updateProfile = (values) => async dispatch => {
    try{
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.patch(`/users/me/update-profile`, values);
        dispatch({ type: types.UPDATE_PROFILE, payload: res.data });
        localStorage.setItem('user', JSON.stringify(res.data.user));
        return res.data;
    }
    catch(error){
        dispatch({ type: types.UPDATE_PROFILE_FAILS, payload: error.response.data })
        return error.response.data;
    }
}

export const updateEmailAddress = (values) => async dispatch => {
    try{
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.patch(`/users/me/update-email`, values);
        dispatch({ type: types.UPDATE_PROFILE, payload: res.data });
        localStorage.setItem('user', JSON.stringify(res.data.user));
        return res.data;
    }
    catch(error){
        dispatch({ type: types.UPDATE_PROFILE_FAILS, payload: error.response.data })
        return  error.response.data;
    }
}

export const updatePassword = (values) => async dispatch => {
    try{
        dispatch({ type: types.USER_REQUEST, payload: true });
        const res = await axios.patch(`/users/me/update-password`, values);
        dispatch({ type: types.UPDATE_PROFILE, payload: res.data });
        localStorage.setItem('user', JSON.stringify(res.data.user));
        return res.data;
    }
    catch(error){
        dispatch({ type: types.UPDATE_PROFILE_FAILS, payload: error.response.data })
        return  error.response.data;
    }
}


export const resetProfile = () => async dispatch => {
    dispatch({ type: types.RESET_PROFILE_STATE});
}
