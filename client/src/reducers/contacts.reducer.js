import { REQUEST_CONTACTS, RESET_CONTACTS_LIST, GET_CONTACTS, GET_CONTACTS_FAILS, DELETE_CONTACT, DELETE_CONTACT_FAILS, SELECT_CONTACT, FILTERING_ITEMS, SEND_CONTACTS, SEND_CONTACTS_FAILS } from '../actions/actionTypes';

const defaultState = {
    headers: [],
    list: {
        name: null,
    },
    items: [],
    selected: [],
    filter: [],
    listSent: false,
    exportReport: null,
    totalCount: 0,
    isDispatching: false,
    success: false,
    error: false
}

export default (state = defaultState, action) => {

    switch (action.type) {

        case REQUEST_CONTACTS:

            return {
                ...state,
                isDispatching: true
            }

        case RESET_CONTACTS_LIST:

            return defaultState

        case GET_CONTACTS:

            return {
                ...state,
                headers: action.payload.list,
                list: action.payload.list,
                items: action.payload.contacts,
                totalCount: action.payload.contacts.length,
                isDispatching: false
            }

        case GET_CONTACTS_FAILS:

            return {
                ...state,
                isDispatching: false,
                error: action.payload.error
            }

        case SEND_CONTACTS:

            return {
                ...state,
                listSent: true,
                exportReport: action.payload.report,
                isDispatching: false
            }

        case SEND_CONTACTS_FAILS:

            return {
                ...state,
                listSent: false,
                isDispatching: false,
                error: action.payload.error
            }

        case SELECT_CONTACT:

            let idAlreadyExists = state.selected.indexOf(action.payload.id) > -1;
            let selected = state.selected.slice();

            if (idAlreadyExists) {
                selected = selected.filter(id => id !== action.payload.id);
            }
            else {
                selected.push(action.payload.id);
            }

            return {
                ...state,
                selected
            }


        case FILTERING_ITEMS:

            let filterAlreadyExists = state.selected.indexOf(action.payload.value) > -1;
            let filter = state.filter.slice();

            if (filterAlreadyExists) {
                filter = filter.filter(value => value !== action.payload.value);
            }
            else {
                filter.push({ [action.payload.value]: action.payload.value });
            }

            return {
                ...state,
                filter
            }

        case DELETE_CONTACT:

            return {
                ...state,
                items: state.items.filter(item => item._id !== action.payload),
                isDispatching: false,
                success: true
            }

        case DELETE_CONTACT_FAILS:

            return {
                ...state,
                isDispatching: false,
                success: false,
                error: action.payload.error
            }


        default:
            return state;
    }

}