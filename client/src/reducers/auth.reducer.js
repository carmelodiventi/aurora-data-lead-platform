import { USER_REQUEST, LOGIN, LOGIN_FAILS, LOGOUT, LOGOUT_FAILS, FORGOT_PASSWORD, FORGOT_PASSWORD_FAILS, RESET_PASSWORD, RESET_PASSWORD_FAILS, CHECK_CODE, CHECK_CODE_FAILS, UPDATE_PROFILE, UPDATE_PROFILE_FAILS, RESET_PROFILE_STATE } from '../actions/actionTypes'

const initialState = {
    user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {},
    error: null,
    success: null,
    hasCode: null,
    codeIsValid: false,
    isLogged: localStorage.getItem('user') ? true : false,
    isDispatching: false
}

export default (state = initialState, action) => {

    switch (action.type) {
        case USER_REQUEST:
            return {
                ...state,
                error: false,
                success: false,
                isDispatching: true
            }
        case LOGIN:
            return {
                ...state,
                user: action.payload.user,
                error: false,
                success: true,
                isLogged: true,
                isDispatching: false
            }
        case LOGIN_FAILS:
            return {
                ...state,
                error: action.payload.error,
                success: null,
                isLogged: false,
                isDispatching: false
            }

        case UPDATE_PROFILE:
            return {
                ...state,
                user: action.payload.user,
                success: true,
                isDispatching: false
            }
        case UPDATE_PROFILE_FAILS:
            return {
                ...state,
                error: action.payload.error,
                success: null,
                isDispatching: false
            }

        case RESET_PROFILE_STATE:

            return {
                ...state,
                error: null,
                success: null
            }

        case FORGOT_PASSWORD:
            return {
                ...state,
                success: action.payload.message,
                hasCode: true,
                isDispatching: false
            }
        case FORGOT_PASSWORD_FAILS:
            return {
                ...state,
                success: null,
                hasCode: false,
                error: action.payload.error,
                isDispatching: false
            }
        case CHECK_CODE:
            return {
                ...state,
                codeIsValid: true,
                success: action.payload.message,
                error: false,
                isDispatching: false
            }
        case CHECK_CODE_FAILS:
            return {
                ...state,
                codeIsValid: false,
                success: null,
                error: action.payload.error,
                isDispatching: false
            }

        case RESET_PASSWORD:
            return {
                ...state,
                success: action.payload.message,
                error: null,
                hasCode: false,
                codeIsValid: false,
                isDispatching: false
            }
        case RESET_PASSWORD_FAILS:
            return {
                ...state,
                success: null,
                hasCode: false,
                codeIsValid: true,
                error: action.payload.error,
                isDispatching: false
            }
        case LOGOUT:
            return {
                ...state,
                user: {},
                success: null,
                error: null,
                isLogged: false,
                isDispatching: false
            }

        case LOGOUT_FAILS:
            return {
                ...state,
                error: action.payload.error,
            }
        default:
            return state
    }
}