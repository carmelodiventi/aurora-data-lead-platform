import { REQUEST_VENDORS, ADD_VENDOR, ADD_VENDOR_FAILS, GET_VENDORS, GET_VENDOR, DELETE_VENDOR, DELETE_VENDOR_FAILS, GET_VENDORS_FAILS, GET_VENDOR_FAILS, UPDATE_VENDOR_FAILS, UPDATE_VENDOR } from '../actions/actionTypes';

const initialState = {
    list: [],
    vendor: {
        name : ''
    },
    error: null,
    isDispatching: false
}

export default function (state = initialState, action) {

    switch (action.type) {

        case REQUEST_VENDORS:

            return {
                ...state,
                isDispatching: true
            }

        case GET_VENDORS:

            return {
                ...state,
                list: action.payload.vendors,
                isDispatching: false
            }

        case GET_VENDORS_FAILS:

            return {
                ...state,
                error: action.payload.error,
                isDispatching: false
            }

        case GET_VENDOR:
            return {
                ...state,
                vendor: action.payload.vendor,
                isDispatching: false
            }

        case GET_VENDOR_FAILS:

            return {
                ...state,
                error: action.payload.error,
                isDispatching: false
            }

        case ADD_VENDOR:

            return {
                ...state,
                list: [...state.list, action.payload.vendor],
                isDispatching: false
            }

        case ADD_VENDOR_FAILS:

            return {
                ...state,
                error: action.payload.error,
                isDispatching: false
            }

        case UPDATE_VENDOR:

            return {
                ...state,
                list: state.list.map(vendor => ({ ...vendor, ...action.payload })),
                isDispatching: false
            }

        case UPDATE_VENDOR_FAILS:

            return {
                ...state,
                error: action.payload.error,
                isDispatching: false
            }

        case DELETE_VENDOR:

            return {
                ...state,
                list: state.list.filter(vendor => vendor._id !== action.payload.vendor._id),
                isDispatching: false
            }

        case DELETE_VENDOR_FAILS:

            return {
                ...state,
                error: action.payload.error,
                isDispatching: false
            }

        default:
            return state
    }

}