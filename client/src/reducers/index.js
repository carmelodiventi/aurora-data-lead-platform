import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import dialogReducer from './dialog.reducer';
import AuthReducer from './auth.reducer';
import ImportReducer from './import.reducer';
import ListReducer from './lists.reducer';
import ContactsReducer from './contacts.reducer';
import TemplatesReducer from './templates.reducer';
import VendorsReducer from './vendors.reducer';
import UsersReducer from './users.reducer';
import LogsReducer from './logs.reducer';

const rootReducer = combineReducers({ 
    auth : AuthReducer,
    import : ImportReducer,
    templates : TemplatesReducer,
    lists : ListReducer,
    contacts : ContactsReducer,
    dialog: dialogReducer,
    vendors: VendorsReducer,
    users: UsersReducer,
    logs: LogsReducer,
    form: formReducer,
});

export default rootReducer;