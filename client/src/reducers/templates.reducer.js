import { REQUEST_TEMPLATES, SAVE_TEMPLATE_FAILS, SAVE_TEMPLATE, GET_TEMPLATES, GET_TEMPLATES_FAILS, GET_TEMPLATE, GET_TEMPLATE_FAILS, RESET_TEMPLATES_FORM, DELETE_TEMPLATE } from '../actions/actionTypes';

const defaultState = {
    list: [],
    template: {},
    isDispatching: false,
    success: false,
    error: ''
}

export default (state = defaultState, action) => {

    switch (action.type) {

        case REQUEST_TEMPLATES:

            return {
                ...state,
                isDispatching: true
            }

        case GET_TEMPLATES:

            return {
                ...state,
                isDispatching: false,
                list: action.payload.templates
            }

        case GET_TEMPLATES_FAILS:

            return {
                ...state,
                isDispatching: false,
                list: [],
                error: action.payload.error
            }


        case GET_TEMPLATE:

            return {
                ...state,
                isDispatching: false,
                template: action.payload.template
            }


        case GET_TEMPLATE_FAILS:

            return {
                ...state,
                isDispatching: false,
                error: action.payload.error
            }


        case SAVE_TEMPLATE:

            return {
                ...state,
                isDispatching: false,
                list: [state.list, action.payload.template],
                success: action.payload.success,
            }

        case SAVE_TEMPLATE_FAILS:

            return {
                ...state,
                isDispatching: false,
                success: false,
                error: action.payload.error
            }


        case DELETE_TEMPLATE:

            return {
                ...state,
                isDispatching: false,
                list: state.list.filter(template => template._id !== action.payload.template._id)
            }

        case DELETE_TEMPLATE:

            return {
                ...state,
                isDispatching: false,
                error: action.payload.error
            }

        case RESET_TEMPLATES_FORM:

            return {
                ...state,
                isDispatching: false,
                success: false,
                error: false
            }


        default:
            return state;
    }

}