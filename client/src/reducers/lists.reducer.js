import { REQUEST_ITEMS, GET_LISTS, GET_LISTS_FAILS, DELETE_ITEM, DELETE_ITEM_FAILS, RESET_LIST_STATE } from '../actions/actionTypes';

const defaultState = {
    items: [],
    selected: [],
    totalCount: 0,
    isDispatching: false,
    success: false,
    error: false
}

export default (state = defaultState, action) => {

    switch (action.type) {

        case REQUEST_ITEMS:

            return {
                ...state,
                isDispatching: true
            }

        case GET_LISTS:

            return {
                ...state,
                items: action.payload.lists.docs,
                totalCount: action.payload.lists.totalDocs,
                isDispatching: false
            }

        case GET_LISTS_FAILS:

            return {
                ...state,
                items: [],
                totalCount: 0,
                isDispatching: false,
                success: false,
                error: action.payload.error
            }

        case DELETE_ITEM:

            return {
                ...state,
                items: state.items.filter(item => item._id !== action.payload.list._id),
                isDispatching: false,
                success: true
            }

        case DELETE_ITEM_FAILS:

            return {
                ...state,
                isDispatching: false,
                success: false,
                error: action.payload.error
            }

        case RESET_LIST_STATE:

        return {
            ...state,
            success: false,
            error: false
        }


        default:
            return state;
    }

}