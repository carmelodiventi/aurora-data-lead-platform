import { SHOW_MODAL, HIDE_MODAL, USER_REQUEST, UPDATE_PROFILE, UPDATE_PROFILE_FAILS } from '../actions/actionTypes';

const initialState = {
    modalProps: {
        open: false
    },
    isDispatching: false
}

export default (state = initialState, action) => {

    switch (action.type) {
        
        case SHOW_MODAL:
            return {
                ...state,
                modalProps: {
                    ...state.modalProps,
                    ...action.payload.modalProps
                }
            }

        case HIDE_MODAL:
            return initialState

        case USER_REQUEST:
            return {
                ...state,
                isDispatching: true
            }

        case UPDATE_PROFILE:
            return {
                ...state,
                modalProps: {
                    open: false
                },
                isDispatching: false
            }
        case UPDATE_PROFILE_FAILS:
            return {
                ...state,
                isDispatching: false
            }


        default:
            return state
    }
}