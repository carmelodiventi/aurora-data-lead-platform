import { REQUEST_IMPORT, SAVE_CONTACTS_LIST, SAVE_CONTACTS_LIST_FAILS, MAPPING_ITEMS, SAVE_CONTACTS, SAVE_CONTACTS_FAILS, GET_CONTACTS_IMPORTED } from '../actions/actionTypes';

const defaultState = {
    items: [],
    fieldsMapped: {},
    isDispatching: false,
    success: false,
    error: ''
}

export default (state = defaultState, action) => {

    switch (action.type) {

        case REQUEST_IMPORT:

            return {
                ...state,
                isDispatching: true,
            }

        case GET_CONTACTS_IMPORTED:

            return {
                ...state,
                items: action.payload.items,
                isDispatching: false,
                success: false
            }

        case SAVE_CONTACTS_LIST:

            return {
                ...state,
                isDispatching: false,
                success: action.payload.success,
                items: [],
                fieldsMapped: []
            }

        case SAVE_CONTACTS_LIST_FAILS:

            return {
                ...state,
                isDispatching: false,
                success: false,
                error: action.payload.error
            }

        case SAVE_CONTACTS:

            return {
                ...state,
                isDispatching: false,
                items: [],
                fieldsMapped: [],
                success: action.payload.success,
            }

        case SAVE_CONTACTS_FAILS:

            return {
                ...state,
                isDispatching: false,
                success: false,
                error: action.payload.error
            }


        case MAPPING_ITEMS:

            return {
                ...state,
                fieldsMapped: {
                    ...state.fieldsMapped,
                    [action.payload.item.index]: action.payload.item.field
                }
            }

        default:
            return state;
    }

}