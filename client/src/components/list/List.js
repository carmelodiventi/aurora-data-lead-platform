import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import ListItem from './ListItem';


const ListWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const ResponsiveTable = styled.div`
    overflow:auto
`;

const Table = styled.table`
    width: 100%;
    border-collapse: separate;
    border-spacing: 1px;
`;

const TableHead = styled.thead`

`;

const TableBody = styled.tbody`

`;

const TableRow = styled.tr`
    border-collapse: separate;
    border-spacing: 1px 1px;
`;

const TableHeadCell = styled.th`
    color: var(--gray);
    padding: .7rem 1rem;
    text-align: left;
`;

const TableCell = styled.td`
    padding: .7rem 1rem;
    white-space: nowrap;
    max-width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    background: var(--white)
`;

const Title = styled.h3`
    margin: 2rem
`;



class List extends Component {


    constructor(props) {
        super(props);

        this.state = {
            rows: [],
            list_name: '',
            list_id: ''
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.rows !== this.props.rows) {
            const { rows } = this.props;
            this.setState({
                rows
            });
        }
    }


    render() {

        const { rows, handleDeleteList, handleSelectItem } = this.props;

        if (rows.length > 0) {

            return (
                <ListWrapper>
                    <ResponsiveTable>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableHeadCell>Name</TableHeadCell>
                                    <TableHeadCell>Created At</TableHeadCell>
                                    <TableHeadCell>Template</TableHeadCell>
                                    <TableHeadCell>Vendor</TableHeadCell>
                                    <TableHeadCell></TableHeadCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map(({ _id, name, template, vendor, created_at }) => (
                                    <ListItem _id={_id} name={name} template={template} vendor={vendor} created_at={created_at} key={_id} handleDeleteList={handleDeleteList} handleSelectItem={handleSelectItem}/>
                                ))}
                            </TableBody>
                        </Table>
                    </ResponsiveTable>
                </ListWrapper>
            )
        }
        else {
            return (
                <Title className="empty-list">
                    No list found...
                </Title>
            )
        }
    }

}

List.propTypes = {
    rows: PropTypes.array.isRequired,
    offset: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired
};

export default List;
