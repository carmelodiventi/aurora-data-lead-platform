import React, { useState } from 'react';
import styled from "styled-components";
import { IoIosSearch, IoIosTrash } from 'react-icons/io';
import { Button } from '../../components/form';

const TableRow = styled.tr`
    border-collapse: separate;
    border-spacing: 1px 1px;
`;

const TableCell = styled.td`
    padding: .7rem 1rem;
    white-space: nowrap;
    max-width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    background: var(--white)
`;

const ListItem = ({ _id, name, template, vendor, created_at, handleSelectItem, handleDeleteList }) => {

    const [isDeleting, setIsDeleting] = useState(false);

    return (
        <TableRow>
            <TableCell component="th" scope="row"> {name} </TableCell>
            <TableCell component="th" scope="row"> {created_at} </TableCell>
            <TableCell component="th" scope="row"> {template?.name} </TableCell>
            <TableCell component="th" scope="row"> {vendor?.name} </TableCell>
            <TableCell component="th" scope="row" align="right">
                {!isDeleting &&
                    <div>
                        <Button className="btn btn-outlined" onClick={() => handleSelectItem(_id)}>
                            <IoIosSearch />
                        </Button>
                        <Button className="btn btn-outlined btn-danger" onClick={() => setIsDeleting(true)}>
                            <IoIosTrash />
                        </Button>
                    </div>
                }
                {isDeleting &&
                    <div>
                        <Button className="btn btn-outlined" onClick={() => setIsDeleting(false)}>
                            Cancel
                        </Button>
                        <Button className="btn btn-outlined btn-danger" onClick={() => handleDeleteList(_id, name)}>
                            Confirm
                        </Button>
                    </div>
                }
            </TableCell>
        </TableRow>
    );
};

export default ListItem;