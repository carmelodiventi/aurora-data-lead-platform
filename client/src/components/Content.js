import React from 'react';
import PrivateRoute from '../PrivateRoute';
import routes from "../Routes";

export default function () {
    return (
      routes.map(({children}) => ( 
          children.map( ({path, component, exact},key) => <PrivateRoute path={path} exact={exact} component={component} key={key} />)
        )
      )
    );
}