import React, { useState } from 'react';
import styled from 'styled-components';
import { Motion, spring } from 'react-motion';
import { IoIosTrash, IoIosColorPalette } from 'react-icons/io';
import { Button } from './form/index';

const Item = styled.div`
    background-color: var(--white);
    padding: 2rem;
    border-radius: var(--border-radius-base);
    border: 1px solid var(--border-color);
    text-align: center;
    transition: all .5s var(--primary-ease);
    position: relative;
   
    &:hover{
        cursor:pointer;
        box-shadow: var(--box-shadow-large);
        z-index: 1;
    }
`;

const ItemBody = styled.div`
    height: 100%;
    width: 100%;
    overflow: hidden;
`;

const ItemIcon = styled.div`
    height: 50px;
    width: 50px;
    background-color: var(--primary-gray);
    border-radius: 100%;
    display: inline-flex;
    align-items:center;
    justify-content: center;
    font-size:2rem;
    color: var(--white);
    overflow: hidden;
    img {
        max-width: 50px
    }
`;

const ItemName = styled.h3`
    text-align: center;
    margin-bottom: .5rem;
`

const ItemDescription = styled.span`
    text-align: center;
    color: var(--gray)
`

const ItemDeleteIcon = styled.span`
    background-color: var(--white);
    padding: 1rem;
    border-radius: var(--border-radius);
    border: 1px solid var(--border-color);
    color: var(--primary-green);
    font-size: 2rem;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(50%,0%);
    opacity: 0;
    visibility: hidden;
    pointer-event: none;
    transition: opacity .95s var(--primary-ease), transform .95s var(--primary-ease);
    border: 1px solid var(--primary-gray);
    &:hover{
        background-color: var(--primary-green);
        color: var(--white);
    }
    ${Item}:hover & {
        transform: translate(50%,-50%);
        opacity: 1;
        visibility: visible;
        pointer-event: all;
    }
`;


const DeleteItem = styled.div`
    position: absolute;
    top: 0;
    left:0;
    bottom:0;
    right:0;

    display: flex;
    align-items: center;
    justify-content: center;
    background: rgba(255,255,255,1);
    opacity: ${props => props.style.opacity};
    transform: translateY(${props => props.style.y}%);

`;

const ListItem = ({ id, name, description, image, onClick, onDelete }) => {

    const [isDeleting, setDeleting] = useState(false);

    const _onConfirm = () => {
        onDelete(id)
    }

    return (
        <Item>
            {!isDeleting &&
                <ItemDeleteIcon>
                    <IoIosTrash onClick={() => setDeleting(!isDeleting)} />
                </ItemDeleteIcon>
            }
            <ItemBody onClick={() => onClick(id)}>
                <ItemIcon>
                    {image ? <img src={image} alt={name} /> : <IoIosColorPalette />}
                </ItemIcon>
                <ItemName>
                    {name}
                </ItemName>
                {description &&
                    <ItemDescription>
                        {description}
                    </ItemDescription>
                }
            </ItemBody>
            {isDeleting &&
                <Motion defaultStyle={{ y: 20, opacity: 0 }} style={{ y: isDeleting ? spring(0) : spring(100), opacity: isDeleting ? spring(1) : spring(0) }}>
                    {interpolatedStyle =>
                        <DeleteItem style={{ y: interpolatedStyle.y, opacity: interpolatedStyle.opacity }}>
                            <Button className="btn btn-primary" onClick={_onConfirm}> Confirm </Button>
                            <Button className="btn btn-outlined" onClick={() => setDeleting(false)}> Cancel </Button>
                        </DeleteItem>
                    }
                </Motion>
            }
        </Item>
    );
};

export default ListItem;