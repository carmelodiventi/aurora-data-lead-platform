import React, { useState, useEffect, memo } from 'react';
import styled from 'styled-components';
import { Checkbox } from '../../form';
import { IoIosCheckmark, IoIosAlert } from 'react-icons/io';

const TableRow = styled.tr`
    border-collapse: separate;
    border-spacing: 1px 1px;
    background-color: ${props => props.isSelected ? 'var(--primary-gray)' : 'var(--white)'}
`;

const TableCell = styled.td`
    padding: .7rem 1rem;
    white-space: nowrap;
`;


const EmailValid = styled.span`
    display: block;
    font-size: 1.5rem;
    color: var(--success-color);
`;

const EmailnotValid = styled.span`
    display: block;
    font-size: 1.5rem;
    color: var(--error-color);
`;

const PhoneValid = styled.span`
    display: block;
    font-size: 1.5rem;
    color: var(--success-color);
`;

const ItemList = ({ item, headers, isSelected, handleSelectItem }) => {

    const [selected, setSelected] = useState(false);

    useEffect(() => {
        setSelected(isSelected)
    }, [isSelected])

   
    return (
        <TableRow isSelected={selected}>
            <TableCell>
                {item.validated && item.validated.email && item.validated.email.isValid ?
                    <EmailValid>
                        <IoIosCheckmark/>
                    </EmailValid>
                    :
                    <EmailnotValid>
                        <IoIosAlert/>
                    </EmailnotValid>
                }
            </TableCell>
            <TableCell padding="checkbox">
                <Checkbox checked={selected} onChange={() => handleSelectItem(item._id)} />
            </TableCell>
            {
                headers.map(({ value }, index) =>
                    <TableCell key={index}>{item[value]}</TableCell>
                )
            }
        </TableRow>
    );
};

export default memo(ItemList);