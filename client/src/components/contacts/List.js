import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import ItemList from './list/ItemList';

const ListWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin-bottom: 75px;
`;

const ResponsiveTable = styled.div`
    overflow:auto
`;

const Table = styled.table`
    border-collapse: separate;
    border-spacing: 1px;
`;

const TableHead = styled.thead`

`;

const TableBody = styled.tbody`

`;

const TableRow = styled.tr`
    border-collapse: separate;
    border-spacing: 1px 1px;
`;

const TableHeadCell = styled.th`
    color: var(--gray);
    padding: .7rem 1rem;
    text-align: left;
    white-space: nowrap;
`;

const Title = styled.h3`
    margin: 2rem;
    padding: 5rem 0;
    display: flex;
    justify-content: center;
`;


class List extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            rows: [],
            headers: []
        }
    }

    componentDidMount() {
        const { rows, headers } = this.props;
        this.setState({
            rows,
            headers
        });
    }

    componentWillReceiveProps(nextProp) {
        if (nextProp.rows !== this.props.rows) {
            const { rows, headers } = nextProp;
            this.setState({
                rows,
                headers
            });
        }
    }

    shouldComponentUpdate(nextProps){
        return nextProps.rows !== this.props.rows || nextProps.selected !== this.props.selected
    }

    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: parseInt(event.target.value)
        })
    }

    isSelected = id => this.props.selected.indexOf(id) !== -1;

    render() {

        const { headers, rows } = this.state;
        const { handleSelectItem, handleDeleteItem } = this.props;

        if (rows.length > 0) {
            return (
                <ListWrapper>
                    <ResponsiveTable>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableHeadCell />
                                    <TableHeadCell />
                                    {headers.map(({ label }, index) =>
                                        <TableHeadCell key={index} data-index={index}>
                                            {label}
                                        </TableHeadCell>
                                    )}
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {rows.map((item, index) => <ItemList item={item} headers={headers} isSelected={this.isSelected(item._id)} handleSelectItem={handleSelectItem} handleDeleteItem={handleDeleteItem} key={`${item.id}-${index}`}/>)}
                            </TableBody>

                        </Table>
                    </ResponsiveTable>
                </ListWrapper>
            )
        }
        else {
            return (
                <Title>
                    No contacts found
                </Title>
            )
        }
    }

}

List.propTypes = {
    rows: PropTypes.array.isRequired
};

export default List;