import React from 'react';
import Select from 'react-select'

const customStyles = {
    menu: (provided, state) => ({
        ...provided,
        width: state.selectProps.width,
        minWidth: "300px",
        color: state.selectProps.menuColor,
        border: 0
    }),
    placeholder: (provided) => ({
        ...provided,
        fontWeight: "bold",
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px solid var(--primary-gray)',
        color: state.isSelected ? 'var(--primary-green)' : 'var(--black)',
        backgroundColor: state.isFocused ? 'var(--primary-gray)' : 'var(--white)',
        padding: ".7rem 1rem",
        fontWeight: state.isSelected ? "bold" : "normal",
    }),
    control: () => ({
        minWidth: "300px",
        display: "flex",
        alignItems: "center",
        padding: ".5rem 1rem",
        marginRight: "1rem",
        border: 0,
        backgroundColor: "var(--white)",
        borderRadius: "var(--border-radius-base)",
        transition: "background .95s var(--primary-ease)"
    }),
    singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = 'opacity 300ms';
        return { ...provided, opacity, transition, fontWeight: "bold" };
    }
}

const TemplateList = ({ list, fieldMapTemplate, handleChangeTemplate }) => {

    const mapOptions = option => {
        return {
            value: option._id,
            label: option.name
        }
    }

    return (
        <Select
            placeholder="Select a Template"
            styles={customStyles}
            defaultValue={fieldMapTemplate}
            options={list.map(option => mapOptions(option))}
            onChange={handleChangeTemplate}
        />
    );
};

export default TemplateList;