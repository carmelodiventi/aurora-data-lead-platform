import React, { useState, useEffect } from 'react';
import { Button } from '../form';
import FiltersMenuItem from './Filters/FiltersMenuItem';
import Side from '../Side';

const FiltersMenu = ({ open, filtersList, handleFilter, handleGetContacts, handleGetFilteredContacts }) => {

    const [filters, setFilters] = useState([]);

    useEffect(() => {

    }, [])

    const handleSelect = e => {
        const { value } = e.target;
        if (!filters.some(filter => filter.item === value)) {
            setFilters([...filters, { item: value }]);
        }
        else {
            setFilters(filters.filter(filter => filter.item !== value));
        }

    }

    const handleChange = e => {
        const { name, value } = e.target;
        const newFilters = filters.map(filter => filter.item === name ? { ...filter, value } : filter);
        setFilters(newFilters);
    }

    const isSelected = value => filters.some(filter => filter.item === value);

    const clearFilter = () => {
        setFilters([])
        handleGetContacts()
    }

    const applyFilter = () => {
        const filtersMapped = {};
        filters.map(({ item, value }) => filtersMapped[item] = value)
        handleGetFilteredContacts(filtersMapped);
    }

    return (


        <Side anchor="left" open={open} handleToggle={handleFilter} closeText="Close Filters">

            <div className="side--content">
                <h4>Filters</h4>
                {
                    filtersList.filter(item => item.filterable === true).map(({ value, label }, index) =>
                        <FiltersMenuItem label={label} value={value} _onSelect={handleSelect} _onChange={handleChange} isSelected={isSelected(value)} key={index}/>
                    )
                }
            </div>

            {filters.length > 0 &&
                <div className="side--footer">
                    <div>
                        <Button className="btn" onClick={applyFilter}> Apply Filter </Button>
                    </div>
                    <div>
                        <Button className="btn" onClick={clearFilter}> Clear Filter </Button>
                    </div>
                </div>
            }

        </Side>

    )

} 

export default FiltersMenu; 