import React, { useEffect } from 'react';
import { Checkbox, FormControl } from '../../form'

const FiltersMenuItem = ({ label, value, isSelected, _onSelect, _onChange }) => {

    useEffect(() => {

    }, [value, isSelected])

    return (
        <FormControl fullWidth>
            <Checkbox
                onChange={_onSelect}
                value={value}
                label={label}
                checked={isSelected}
            />
            {isSelected &&
                <input type="text" onChange={_onChange} name={value}/>
            }
        </FormControl>
    );
};

export default FiltersMenuItem;