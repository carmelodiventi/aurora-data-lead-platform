import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormControl, Button } from '../../form';

const validate = values => {
    const errors = {}
    const requiredFields = [
        'url'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.url && !/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g.test(values.url)
    ) {
        errors.url = 'Invalid web address'
    }
    return errors
}

const renderField = (field) => (
    <FormControl {...field} error={field.meta.touched && field.meta.error}>
        {field.label && <label>{field.label}</label>}
        <input {...field.input} type={field.type} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)

const ExportForm = props => {
    const { handleSubmit, pristine, submitting, invalid } = props;

    return (
        <form onSubmit={handleSubmit}>

            <Field
                fullWidth
                required
                id="url"
                label="Export via Url"
                name="url"
                type="text"
                component={renderField}
            />

            <FormControl fullWidth>
                <Button className="btn btn-primary btn-block" disabled={pristine || submitting || invalid}> Send Contacts </Button>
            </FormControl>

        </form>
    );
};

export default reduxForm({ form: 'ExportForm', validate })(ExportForm)