import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';


const DropDownMenuContainer = styled.div`
  position:relative;
  cursor: pointer;
  height: 100%;
  margin-left: 1rem;
  &:hover{
    .dropdown-content {
        opacity: 1;
        visibility: visible;
        transform: translateY(0rem);
        pointer-events: all;
    }
  }
`;

const Menu = styled.div`
    min-width: 100px;
    position:absolute;
    top:100%;
    right:0;
    z-index: 1;
    opacity: 0;
    visibility: hidden;
    transform: translateY(1.2rem);
    pointer-events: none;
    background-color: white;
    border-radius: 5px;
    box-shadow: var(--box-shadow-base);
    overflow:hidden;
    transition: opacity .4s ease-out, transform .5s ease-out; 
`;

const MenuItems = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;
`;


const MenuTitle = styled.div`
    font-weight: bold;
    padding: 0.5rem 1rem;
    border-bottom: 1px solid var(--primary-gray);
    color: var(--primary-font-color)
`;

const MenuItem = styled.li`
    + li{
        border-top: 1px solid var(--primary-gray);
    }
    transition: background .5s linear;
    &:hover{
        background: var(--primary-gray);
    }
    a,span{
        width:100%;
        display:block;
        padding: 0.5rem 1rem;
        text-decoration: none;
        background: none;
        border: none;
        font-weight: normal;
        color: var(--primary-font-color);
        &:visited{
            color: var(--primary-font-color);
        }
        &:hover,
        &:active{
            color: var(--primary-color);
            cursor:pointer
        }
    }
`;


const DropDownMenu = ({children, title, items}) => {
    return (
        <DropDownMenuContainer>
            {children}
          <Menu className="dropdown-content">
            {title && <MenuTitle> {title} </MenuTitle>}
            <MenuItems>
                {items.map(({label, link, action}) => {
                    return (
                        <MenuItem key={label}>
                            {link && <Link to={link}> {label}  </Link>}
                            {action && <span onClick={action}> {label}  </span>}
                        </MenuItem>
                    )
                })}
            </MenuItems>
          </Menu>
        </DropDownMenuContainer>
    );
};

DropDownMenu.defaultProps = {
    title: ''
}

export default DropDownMenu;