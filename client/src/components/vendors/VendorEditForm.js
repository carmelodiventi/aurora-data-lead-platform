import React, { Component, createRef } from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormControl, Button } from '../form';
import { IoIosCloudUpload, IoIosImage } from 'react-icons/io';


const renderField = ({ input, meta, label, type }) => (
    <FormControl fullWidth error={meta.touched && meta.error}>
        {label && <label> {label} </label>}
        <input {...input} type={type} />
        {meta.touched && meta.error &&
            <span className="error">{meta.error}</span>
        }
    </FormControl>
)

const renderFileField = ({ input: { value: omitValue, ...inputProps }, label, meta: omitMeta, touched, error, ...props }) => (
    <FormControl fullWidth error={touched && error}>
        {label && <label htmlFor={props.id} className="custom-file-upload"> {label} </label>}
        <input {...inputProps} {...props}/>
        {touched && error &&
            <span className="error">{error}</span>
        }
    </FormControl>

);

const validate = values => {
    const errors = {}
    const requiredFields = [
        'name'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    return errors
}


class VendorEditForm extends Component {


    render() {

        const { handleSubmit, pristine, submitting } = this.props;

        return (

            <form noValidate onSubmit={handleSubmit}>

                <Field
                    required
                    name="name"
                    label="Company Name"
                    type="text"
                    id="name"
                    component={renderField}
                />

                <Field
                    required
                    name="logo"
                    label="Company Logo"
                    type="file"
                    value={null}
                    id="logo"
                    component={renderFileField}
                />

                <FormControl fullWidth>
                    <Button
                        type="submit"
                        variant="contained"
                        color='primary'
                        disabled={pristine || submitting}>
                        Edit Vendor
                    </Button>
                </FormControl>

            </form>

        )
    }

}

export default reduxForm({ form: 'VendorEditForm', enableReinitialize: true, validate })(VendorEditForm)