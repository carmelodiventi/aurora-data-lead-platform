import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormControl, Button } from '../form';


const renderField = (field) => (
    <FormControl fullWidth error={field.meta.touched && field.meta.error}>
        {field.label && <label>{field.label}</label>}
        <input {...field.input} type="text" />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)

const validate = values => {
    const errors = {}
    const requiredFields = [
        'name'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    return errors
}


class VendorForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value })
    }

    render() {

        const { handleSubmit, pristine, submitting } = this.props;

        return (

            <form noValidate onSubmit={handleSubmit}>

                <Field
                    id="name"
                    label="Company Name"
                    name="name"
                    onChange={this.handleChange}
                    component={renderField}
                />

                <FormControl fullWidth>
                    <Button
                        type="submit"
                        variant="contained"
                        color='primary'
                        disabled={pristine || submitting}>
                        Add Vendor
                    </Button>
                </FormControl>

            </form>

        )
    }

}

export default reduxForm({ form: 'VendorForm', validate })(VendorForm)