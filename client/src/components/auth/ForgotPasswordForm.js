import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, FormControl } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'email',
        'password'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    return errors
}

const renderTextField = ({
    label,
    input,
    meta: { touched, invalid, error },
    ...custom
}) => (
        <FormControl fullWidth error={touched && error}>
            {label && <label htmlFor={custom.id}>{label}</label>}
            <input {...input} {...custom} type={custom.type} />
            {touched && error &&
                <span className="error">{error}</span>
            }
        </FormControl>
    )


const ForgotPasswordForm = ({ handleSubmit, pristine, submitting }) => {


    return (

        <form onSubmit={handleSubmit}>

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                type="email"
                name="email"
                autoComplete="email"
                component={renderTextField}
            />

            <FormControl fullWidth className="form-control">

                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={pristine || submitting}
                >
                    Reset Password
                </Button>

            </FormControl>

            <FormControl fullWidth>
                <Link to="/login" variant="body2">
                    Have you the login data?
                </Link>
            </FormControl>

        </form>

    )
}

export default reduxForm({ form: 'ForgotPasswordForm', validate })(ForgotPasswordForm)