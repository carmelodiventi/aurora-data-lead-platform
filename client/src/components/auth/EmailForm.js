import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, FormControl } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'email'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }

    return errors
}


const renderField = (field) => (
    <FormControl {...field} error={field.meta.touched && field.meta.error}>
        {field.label && <label>{field.label}</label>}
        <input {...field.input} type={field.type} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)


const EmailForm = ({ handleSubmit, pristine, submitting, invalid }) => {


    return (

        <form onSubmit={handleSubmit}>

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="email"
                label="Email address"
                type="email"
                id="email"
                autoComplete="email"
                component={renderField}
            />

            <FormControl fullWidth>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={pristine || submitting || invalid}
                >
                    Update
                </Button>
            </FormControl>
        </form>

    )
}

export default reduxForm({ form: 'EmailForm', validate })(EmailForm)