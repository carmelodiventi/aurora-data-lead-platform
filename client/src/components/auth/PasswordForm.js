import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormControl, Button } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'oldPassword',
        'confirmPassword',
        'password'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
   
    if (
        values.oldPassword && values.oldPassword.length < 8
    ) {
        errors.oldPassword = 'Password is too short'
    }

    if (
        values.password && values.password.length < 8
    ) {
        errors.password = 'Password is too short'
    }

    if (
        values.password !== values.confirmPassword
    ) {
        errors.confirmPassword = 'Password and Confirm Password are not matching'
    }

    return errors
}


const renderField = (field) => (
    <FormControl {...field} error={field.meta.touched && field.meta.error}>
        {field.label && <label>{field.label}</label>}
        <input {...field.input} type={field.type} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)


const PasswordForm = ({ handleSubmit, pristine, submitting, invalid }) => {


    return (

        <form onSubmit={handleSubmit}>
            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="oldPassword"
                label="Old Password"
                type="password"
                id="oldPassword"
                autoComplete="old-password"
                component={renderField}
            />

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                component={renderField}
            />

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="confirmPassword"
                label="Confirm Password"
                type="password"
                id="confirmPassword"
                autoComplete="confirm-password"
                component={renderField}
            />
            <FormControl fullWidth>
                <Button type="submit" disabled={pristine || submitting || invalid}>
                    Update
                </Button>
            </FormControl>
        </form>
 
    )
}

export default reduxForm({ form: 'PasswordForm', validate })(PasswordForm)