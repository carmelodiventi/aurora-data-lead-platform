import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, FormControl } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'code'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (values.code && /^.{5}$/.test(values.code)) {
        errors.email = 'Invalid code length'
    }
    return errors
}

const renderTextField = ({
    label,
    input,
    meta: { touched, invalid, error },
    ...custom
}) => (
        <FormControl fullWidth error={touched && error}>
            {label && <label htmlFor={custom.id}>{label}</label>}
            <input {...input} {...custom} type={custom.type} />
            {touched && error &&
                <span className="error">{error}</span>
            }
        </FormControl>
    )

const CheckCodeForm = props => {
    const { handleSubmit, pristine, submitting } = props;


    return (

        <form onSubmit={handleSubmit}>

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="code"
                label="Code"
                name="code"
                type="text"
                component={renderTextField}
            />

            <FormControl fullWidth className="form-control form-group">

                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={pristine || submitting}
                >
                    Request New Password
                </Button>
            </FormControl>
            <FormControl fullWidth className="form-control">
                <Link to="/forgot-password">
                    Have you not received the code yet?
                </Link>
            </FormControl>
        </form>

    )
}

export default reduxForm({ form: 'CheckCodeForm', validate })(CheckCodeForm)