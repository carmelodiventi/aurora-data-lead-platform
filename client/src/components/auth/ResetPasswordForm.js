import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, FormControl } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'password',
        'confirmPassword'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.password !== values.confirmPassword
    ) {
        errors.confirmPassword = 'The password is not matching with the Password above'
    }
    return errors
}

const renderTextField = ({
    label,
    input,
    meta: { touched, invalid, error },
    ...custom
}) => (
        <FormControl fullWidth error={touched && error}>
            {label && <label htmlFor={custom.id}>{label}</label>}
            <input {...input} {...custom} type={custom.type} />
            {touched && error &&
                <span className="error">{error}</span>
            }
        </FormControl>
    )


const ResetPasswordForm = props => {

    const { handleSubmit, pristine, submitting } = props;

    return (

        <form onSubmit={handleSubmit}>

            <Field
                variant="outlined"
                required
                type="password"
                id="password"
                label="Password"
                name="password"
                autoComplete="password"
                component={renderTextField}
            />

            <Field
                variant="outlined"
                required
                type="password"
                id="confirmPassword"
                label="Confirm Password"
                name="confirmPassword"
                autoComplete="confirmPassword"
                component={renderTextField}
            />

            <FormControl fullWidth className="form-control">
                <Button type="submit" disabled={pristine || submitting}>
                    Reset Password
                </Button>
            </FormControl>

            <FormControl fullWidth className="form-control">
                <Link to="/login">
                    Have you the login data?
                </Link>
            </FormControl>

        </form>

    )
}

export default reduxForm({ form: 'ResetPasswordForm', validate })(ResetPasswordForm)