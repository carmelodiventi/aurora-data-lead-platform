import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Checkbox, Button, FormControl } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'email',
        'password'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    return errors
}

const renderTextField = ({
    label,
    input,
    meta: { touched, invalid, error },
    ...custom
}) => (
        <FormControl fullWidth error={touched && error}>
            {label && <label htmlFor={custom.id}>{label}</label>}
            <input {...input} {...custom} type={custom.type} />
            {touched && error &&
                <span className="error">{error}</span>
            }
        </FormControl>
    )

const renderCheckbox = ({ input, label }) => (

    <Checkbox
        label={label}
        checked={input.value ? true : false}
        onChange={input.onChange}
    />

)


const LoginForm = props => {

    const { handleSubmit, pristine, submitting } = props;

    return (

        <form onSubmit={handleSubmit}>

            <Field
                variant="outlined"
                margin="normal"
                required
                id="email"
                label="Email Address"
                name="email"
                type="email"
                autoComplete="email"
                component={renderTextField}
            />

            <Field
                variant="outlined"
                margin="normal"
                required
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                component={renderTextField}
            />


            <FormControl fullWidth className="form-control form-group">
                <Field name="remember" component={renderCheckbox} label="Remember me" />
                <Button type="submit" disabled={pristine || submitting}>
                    Sign In
                </Button>
            </FormControl>

            <FormControl fullWidth>
                <Link to="/forgot-password">
                    Forgot password?
                </Link>
            </FormControl>

        </form>

    )
}

export default reduxForm({ form: 'LoginForm', validate })(LoginForm)