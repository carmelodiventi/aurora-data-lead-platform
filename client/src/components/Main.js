import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Navigation from './Navigation';

const AppContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const MainContent = styled.main`
  min-height: 100vh;
  padding: 75px 0 0 75px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0;
`;

class Main extends React.Component {

  state = {
    mobileOpen: false,
  };

  handleMenuToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  render() {

    const { children } = this.props;

    return (
      <>
        <AppContent>
          <Navigation />
          <MainContent> 
            <Container>
              {children}
            </Container>
          </MainContent>
        </AppContent>
      </>
    );
  }
}

export default Main;