import React from 'react';
import styled from 'styled-components';

const BlockItem = styled.div`
    background-color: var(--white);
    padding: 2rem;
    border-radius: var(--border-radius-base);
    border: 1px solid var(--primary-gray);
    + .block {
        margin-top: 2rem;
    }
`;

const BlockHeader = styled.div`
    display:grid;
    grid-template-columns: 100px  1fr auto;
    grid-gap: 2rem;
`;

const BlockIcon = styled.div`
    height: 100px;
    width: 100px;
    background-color: var(--primary-green);
    padding:1rem;
    border-radius: 100%;
    display: flex;
    align-items:center;
    justify-content: center;
    font-size:2rem;
    color: var(--white);
`;

const BlockText = styled.div`
    display: flex;
    flex-direction: column;
`;

const BlockOptions = styled.div`
    display: flex;
    align-items:center;
    justify-content: center;
`;


const BlockTitle = styled.h3`
    margin-bottom: 0;
`;

const BlockDescription = styled.p`
    color: var(--dark-gray);
`;

const BlockBody = styled.div`
    display:block;
    margin-top: 2rem;
`;

const Block = ({ title, description, icon, options, children }) => {
    return (
        <BlockItem className="block">
            <BlockHeader>
                <BlockIcon>
                    {icon}
                </BlockIcon>
                <BlockText>
                    <BlockTitle>
                        {title}
                    </BlockTitle>
                    <BlockDescription>
                        {description}
                    </BlockDescription>
                </BlockText>
                {options &&
                    <BlockOptions>
                        {options}
                    </BlockOptions>
                }
            </BlockHeader>
            {children &&
                <BlockBody>
                    {children}
                </BlockBody>
            }
        </BlockItem>
    );
};


export default Block;