import React from 'react';
import styled from "styled-components";
import { Button } from '../form';
import { IoIosEye } from 'react-icons/io';
import Modal from 'react-modal';

const TableRow = styled.tr`
    border-collapse: separate;
    border-spacing: 1px 1px;
`;

const TableCell = styled.td`
    padding: .7rem 1rem;
    background: var(--white)
`;

const customStyles = {
    content: {
        border: '1px solid var(--border-color)',
        maxWidth: '800px',
        maxHeight: '100vh',
        overflow: 'hidden',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

const ListItem = ({ user, operation, details, created_at }) => {

    const [modalIsOpen, setIsOpen] = React.useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <>
            <TableRow>
                <TableCell component="th" scope="row"> {user?.email} </TableCell>
                <TableCell component="th" scope="row"> {operation} </TableCell>
                <TableCell component="th" scope="row"> {created_at} </TableCell>
                <TableCell component="th" scope="row">  <Button onClick={openModal}> <IoIosEye /> </Button> </TableCell>
            </TableRow>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
            >

                <h2>Logs Details </h2>
                {JSON.stringify(details)}
            </Modal>
        </>
    );
};

export default ListItem;