import React from 'react';
import styled from 'styled-components';

const Spin = styled.div`
    height:50px;
    width:50px;
    border: 2px solid var(--primary-green);
    border-top: 2px solid var(--green);
    border-radius: 50%;
    animation: animateCircularSpin 1s infinite linear;

    @keyframes animateCircularSpin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`;

const CircularSpinner = ({ className }) => <Spin className={className} />;

export default CircularSpinner;