import React from 'react';
import styled from 'styled-components';

const RadioboxContainer = styled.div`
    display: flex;
    align-items: center;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 1rem;
    user-select: none;

    input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    label {
        margin: 0;
    }

    .dot {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: var(--primary-gray);
        border-radius: 50%;
        &:after {
            content: "";
            position: absolute;
            display: none;
        }
    }

    &:hover input ~ .dot {
        background-color: var(--gray);
    }

    input:checked ~ .dot {
        background-color: var(--primary-green);
        &:after {
            display: block;
        }
    }

    .dot:after {
        left: 50%;
        top: 50%;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        background: white;
        transform: translate(-50%,-50%);
    }

`;


export const Radiobox = ({ ...props }) => (
    <RadioboxContainer>
        <label htmlFor={props.id}>
            {props.label}
            <input type="radio" {...props} value={props.id}/>
            <span className="dot" />
        </label>
    </RadioboxContainer>
)