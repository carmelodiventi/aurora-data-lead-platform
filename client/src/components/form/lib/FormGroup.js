import styled from 'styled-components';

export const FormGroup = styled.div`
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    > * {
        + * {
            margin-left: 1rem
        }
    }
`;
