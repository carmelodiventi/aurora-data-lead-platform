import styled from 'styled-components';

export const Divider = styled.hr`
    margin: 2rem 0;
    border-top: 1px solid var(--primary-gray);
`;