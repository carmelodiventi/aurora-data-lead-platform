import React from 'react';
import styled from 'styled-components';

const FormControlItem = styled.div`
    width: ${props => props.fullWidth ? "100%" : "auto"};
    display: ${props => props.fullWidth ? "block" : "inline-block"};
`;

export const FormControl = (props) => {
    return (
        <FormControlItem className={`form-control ${props.error ? 'has-error' : ''}`} {...props}>
            {props.children}
        </FormControlItem>
    );
};