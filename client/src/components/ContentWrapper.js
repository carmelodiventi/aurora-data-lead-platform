import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    width:100%;
    height: 100%;
`;

const ContentWrapper = ({ children }) => {
    return (
        <Container>
            {children}
        </Container>
    );
};

export default ContentWrapper;