// @Flow

import React from 'react';
import { Button } from '../components/form';
import { IoIosArrowBack } from 'react-icons/io';

const Side = ({ children, anchor, open, closeText, handleToggle }) => {
    return (
        <div className={`side side-${anchor} side-${open}`}>
            <div className="side--header">
                <Button color="primary" onClick={handleToggle}>
                    <IoIosArrowBack/> {closeText}
                </Button>
            </div>
            {children}
        </div>
    );
};

Side.defaultProps = {
    closeText : 'close'
}

export default Side;