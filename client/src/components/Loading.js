import React from 'react';
import styled from 'styled-components';
import CircularSpinner from './CircularSpinner';

const LoadingContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: rgba(255,255,255,.9);
    z-index:999;
    
    .loadingProgress{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }

`;


const Loading = ({ showing }) => {
    if (showing) {
        return (
            <LoadingContainer>
                <CircularSpinner className="loadingProgress" />
            </LoadingContainer>
        );
    }

    return null
};

export default Loading;