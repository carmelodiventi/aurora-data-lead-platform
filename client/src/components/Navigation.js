import React from 'react';
import { connect } from 'react-redux';
import { NavLink, Link } from "react-router-dom";
import { IoIosAnalytics, IoIosSwap, IoIosPeople, IoIosListBox, IoIosColorPalette, IoIosCart, IoIosCube, IoIosDocument } from 'react-icons/io';
import styled from 'styled-components';
import Logo from './Logo';

const routes = [
  { label: 'Home', icon: <IoIosAnalytics />, path: '/', exact: true, role: ['admin', 'client'] },
  { label: 'Import', icon: <IoIosSwap />, path: '/import', exact: true, role: ['admin'] },
  { label: 'Templates', icon: <IoIosColorPalette />, path: '/templates', exact: true, role: ['admin'] },
  { label: 'Vendors', icon: <IoIosCube />, path: '/vendors', exact: true, role: ['admin'] },
  { label: 'Users', icon: <IoIosPeople />, path: '/users', exact: true, role: ['admin'] },
  { label: 'Lists', icon: <IoIosListBox />, path: '/lists', exact: true, role: ['admin', 'client'] },
  { label: 'Logs', icon: <IoIosDocument />, path: '/logs', exact: true, role: ['admin'] }
];


const Nav = styled.nav`
    background-color: white;
    height: 100vh;
    width: 75px;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 6;
    border-right: 1px solid var(--border-color);
    > a {
      width:72px;
      height: 72px;
      align-items: center;
      justify-content: center;
      display: flex;
      font-size: 1.8rem;
      transition: background .5s linear;
      background-color: white;
      position: relative;
      &,
      &:active,
      &:hover,
      &:visited{
        color: var(--black);
      }
      &:hover{
        label{
          opacity: 1;
          visibility:visible
        }
      }
      svg{
        fill: currentColor;
      }
      &.logo{
        padding: 0;
        margin: 0;
        border-radius: 0;
        img{
          max-width:2.5rem
        }
      }
      &.active{
        background-color: var(--primary-gray);
        color: var(--primary-green);
      }
    }
`;

const Label = styled.label`
    position: absolute;
    font-size: 1rem;
    left: calc(100%);
    background: rgba(0,0,0,.8);
    color: white;
    padding: .5rem;
    border-radius: .25rem;
    visibility:hidden;
    opacity:0
`;

const Navigation = ({ auth }) => {

  const filteredRoutes = routes.filter(route => {
    return route.role.includes(auth.user.role);
  })

  return (
    <Nav>
      <Link to="/" className="logo">
        <Logo />
      </Link>
      {filteredRoutes.map(({ label, icon, path, exact }) => (
        <NavLink to={path} exact={exact} key={label}>
          {icon} <Label>{label}</Label>
        </NavLink>
      ))}
    </Nav>
  );
}

export default connect(state => ({ auth: state.auth }))(Navigation)