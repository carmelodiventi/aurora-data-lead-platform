import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Motion, spring } from 'react-motion';
import { IoIosClose } from 'react-icons/io';

const DialogContainer = styled.div`
    height: 100vh;
    width: 100vw;
    position:fixed;
    top: 0;
    left:0;
    z-index: 15;
    background: var(--white);
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    transform: translateX(${props => props.style.x}%)
`;

const DialogClose = styled.span`
    position:absolute;
    top:2rem;
    right:2rem;
    font-size:2.2rem;
    &:hover{
        cursor:pointer;
    }
`;


const DialogTitle = styled.h2`
    
`;

const DialogBody = styled.div`
    margin-top: 0rem
`;

const DialogDescription = styled.h3`
    
`;


const Dialog = ({ title, description, children, open, onCancel, loading, success, error }) => {

    return (
        <Motion defaultStyle={{ x: 100 }} style={{ x: open ? spring(0) : spring(100) }}>
            {interpolatedStyle =>
                <DialogContainer style={{ x: interpolatedStyle.x }}>
                    <DialogTitle>
                        {title}
                    </DialogTitle>
                    <DialogClose onClick={onCancel}>
                        <IoIosClose />
                    </DialogClose>
                    <DialogBody>
                        <DialogDescription>
                            {description}
                        </DialogDescription>
                        {children}
                    </DialogBody>
                </DialogContainer>
            }
        </Motion>
    );
};

Dialog.propsType = {
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    success: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired
}

Dialog.defaultProps = {
    title: 'Title'
}

export default Dialog;