import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io'
import FieldsMapperDropdown from './FieldsMapperDropdown';
import Pagination from 'react-paginate';

const ListWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const ResponsiveTable = styled.div`
    overflow:auto
`;

const Table = styled.table`
`;

const TableHead = styled.thead`

`;

const TableBody = styled.tbody`

`;

const TableRow = styled.tr`
    border-collapse: separate;
    border-spacing: 1px 1px;
`;

const TableHeadCell = styled.th`
    color: var(--gray);
    padding: .7rem 1rem;
    text-align: left;
`;

const TableCell = styled.td`
    padding: .7rem 1rem;
    white-space: nowrap;
    max-width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    background: var(--white)
`;

const Title = styled.h3`
    margin: 2rem;
    padding: 5rem 0;
    display: flex;
    align-items: center;
    justify-content: center;
    color: var(--text-color-secondary)
`;



class List extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            page: 0,
            rowsPerPage: 100,
            rows: [],
            rowsPerPageOptions: []
        }
    }

    componentDidMount() {
        const { rows } = this.props;
        this.setState({
            rows
        });
    }

    componentWillReceiveProps(nextProp) {
        if (nextProp.rows !== this.props.rows) {
            const { rows } = nextProp;
            this.setState({
                rows
            });
        }
    }

    handleChangePage = ({ selected }) => {
        this.setState({
            page: selected
        })
    }

    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: parseInt(event.target.value)
        })
    }

    handleChangeMapField = (item) => {
        this.props.handleMapFields(item);
    }

    tableMapCell = (row, index) => {
        let rowCell = [];
        for (let value of row) {
            rowCell.push(<TableCell key={index++}> {value} </TableCell>)

        }
        return rowCell;
    }


    render() {

        const { rows, page, rowsPerPage } = this.state;
        const { fieldsMapped, fieldsMapTemplateItem, fieldMapTemplate } = this.props;
        const [headers, ...items] = rows;


        if (this.state.rows.length > 0) {
            return (
                <ListWrapper>
                    <ResponsiveTable>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    {headers.map((item, index) =>
                                        <TableHeadCell key={index}>{item}</TableHeadCell>
                                    )}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    {
                                        headers.map((item, index) => (
                                            <TableCell key={index}>
                                                <FieldsMapperDropdown
                                                    fieldsMapTemplateItem={fieldsMapTemplateItem}
                                                    fieldsMapped={fieldsMapped}
                                                    fieldMapTemplate={fieldMapTemplate}
                                                    handleChangeMapField={this.handleChangeMapField}
                                                    item={item}
                                                    index={index}
                                                />
                                            </TableCell>
                                        )
                                        )
                                    }
                                </TableRow>
                                {items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => (
                                <TableRow key={index}>
                                    {this.tableMapCell(row, index)}
                                </TableRow>
                                ))}
                            </TableBody>
                            
                        </Table>
                    </ResponsiveTable>
                    {items.length > rowsPerPage &&
                     
                        <Pagination
                            containerClassName="pagination"
                            previousLabel={<IoIosArrowBack/>}
                            nextLabel={<IoIosArrowForward/>}
                            initialPage={page} 
                            onPageChange={this.handleChangePage} 
                            pageRangeDisplayed={5} 
                            marginPagesDisplayed={2} 
                            pageCount={items.length / rowsPerPage} 
                        />

                    }
                </ListWrapper>
            )
        }
        else {
            return (
                <Title className="empty-list">
                    No contacts imported yet...
                </Title>
            )
        }
    }

}

List.propTypes = {
    rows: PropTypes.array.isRequired
};

export default List;