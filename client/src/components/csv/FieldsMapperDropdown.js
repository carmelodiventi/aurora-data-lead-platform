import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { MenuItem, Select } from '@material-ui/core';
import { levenshteinDistance } from '../../algorithms/string/levenshtein-distance/levenshteinDistance'

const FieldsMapperDropdown = ({ item, index, handleChangeMapField, fieldsMapped, fieldsMapTemplateItem, fieldMapTemplate }) => {

    const [field, setField] = useState('');

    useEffect(() => {
        autoMapField()
    }, [fieldMapTemplate])

    const autoMapField = () => {

        // Getting the closest word comparing a and b with Levenshtein distance algorithm
        
        fieldsMapTemplateItem.map(async ({ label, value }) => {

            const closestValue = levenshteinDistance(item, label);

            if (0 <= closestValue && closestValue <= 1){
                setField(value);
                await handleChangeMapField({
                    field: value,
                    index
                });
            }

        })

    }

    const onChange = (e) => {

        const value = e.target.value;

        const item = {
            field: value,
            index: index
        }

        setField(value);
        handleChangeMapField(item);
    }

    const disabledOptions = value => {

        for (let k in fieldsMapped) {
            if (fieldsMapped[k] === value) {
                return true;
            }
        }
        return false
    }

    return (
        <Select
            value={field}
            name={item}
            style={{ 'width': '100%' }}
            onChange={e => onChange(e)}>
            <MenuItem value="">
                Select a Value
            </MenuItem>
            {
                fieldsMapTemplateItem.map(({ label, value }) => (
                    <MenuItem key={label} value={value} disabled={disabledOptions(value)}>
                        {label}
                    </MenuItem>
                ))
            }
        </Select>
    )

}

FieldsMapperDropdown.propTypes = {
    item: PropTypes.string,
    index: PropTypes.number
}

export default FieldsMapperDropdown;