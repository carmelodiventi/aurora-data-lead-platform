import React from 'react';
import styled from 'styled-components';


const AvatarContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    align-items:center;
`

const AvatarIcon = styled.div`
    height:36px;
    width:36px;
    background-color: var(--primary-gray);
    display: flex;
    align-items:center;
    justify-content:center;
    border-radius:100%;
    font-weight:bold
    text-transform: uppercase;
`;

const Avatar = ({ initial }) => {
    return (
        <AvatarContainer>
            <AvatarIcon>
                {initial}
            </AvatarIcon>
        </AvatarContainer>
    );
};

export default Avatar;