import React from 'react';

const UserInfo = ({ info : { firstname, email } }) => {
    return (
        <div className="user--info">
            <strong>{firstname || email}</strong>
        </div>
    );
};

export default UserInfo;