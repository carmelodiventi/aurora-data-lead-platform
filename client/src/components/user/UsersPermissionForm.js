import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormControl, Button, Divider, Checkbox } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'firstname',
        'lastname',
        'email'
    ]

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Add a valid email address'
    }

    return errors
}

const renderField = (field) => {

    return (
        <FormControl fullWidth error={field.meta.touched && field.meta.error}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}
            <input {...field.input} type={field.type} />
            {field.meta.touched && field.meta.error &&
                <span className="error">{field.meta.error}</span>
            }
        </FormControl>
    )
}


const renderCheckBox = (field) => (

    <FormControl fullWidth error={field.meta.touched && field.meta.error}>
        <Checkbox label={field.label} {...field.input} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)


class UsersPermissionForm extends Component {

    render() {

        const { handleSubmit, pristine, submitting } = this.props;

        return (
            <form onSubmit={handleSubmit}>

                <Field
                    id="firstname"
                    label="First Name"
                    name="firstname"
                    type="text"
                    required
                    component={renderField}
                />

                <Field
                    id="lastname"
                    label="Last Name"
                    name="lastname"
                    type="text"
                    required
                    component={renderField}
                />

                <Field
                    id="username"
                    label="Username"
                    name="username"
                    type="text"
                    required
                    component={renderField}
                />

                <Field
                    id="email"
                    label="Email"
                    name="email"
                    type="email"
                    required
                    component={renderField}
                />

                <Field
                    id="phone"
                    label="Phone"
                    name="phone"
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    required
                    component={renderField}
                />

                <Field
                    id="isAdmin"
                    label="Admin"
                    name="admin"
                    type="checkbox"
                    component={renderCheckBox}
                />

                <Field
                    id="isActive"
                    label="Active"
                    name="is_active"
                    type="checkbox"
                    component={renderCheckBox}
                />


                <Divider />

                <FormControl fullWidth>
                    <Button type="submit" disabled={submitting || pristine}>
                        Save User
                    </Button>
                </FormControl>

            </form>
        )
    }

}


export default reduxForm({ form: 'UsersPermissionForm', validate, enableReinitialize: true })(UsersPermissionForm)