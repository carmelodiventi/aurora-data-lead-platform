import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Field, formValueSelector, reduxForm } from 'redux-form';
import { FormControl, Button, Divider, Checkbox } from '../form';
import Select from 'react-select';

const validate = values => {
    const errors = {}
    const requiredFields = [
        'firstname',
        'lastname',
        'email'
    ]

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Add a valid email address'
    }

    return errors
}

const renderField = (field) => {

    return (
        <FormControl fullWidth error={field.meta.touched && field.meta.error}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}
            <input {...field.input} type={field.type} />
            {field.meta.touched && field.meta.error &&
                <span className="error">{field.meta.error}</span>
            }
        </FormControl>
    )
}


const renderCheckBox = (field) => (

    <FormControl fullWidth error={field.meta.touched && field.meta.error}>
        <Checkbox label={field.label} {...field.input} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)

const customStyles = {
    menu: (provided, state) => ({
        ...provided,
        width: state.selectProps.width,
        minWidth: "300px",
        color: state.selectProps.menuColor,
        border: 0
    }),
    placeholder: (provided) => ({
        ...provided,
        fontWeight: "bold",
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px solid var(--primary-gray)',
        color: state.isSelected ? 'var(--primary-green)' : 'var(--black)',
        backgroundColor: state.isFocused ? 'var(--primary-gray)' : 'var(--white)',
        padding: ".7rem 1rem",
        fontWeight: state.isSelected ? "bold" : "normal",
    }),
    control: () => ({
        minWidth: "300px",
        display: "flex",
        alignItems: "center",
        padding: ".5rem 1rem",
        marginRight: "1rem",
        border: "1px solid var(--border-input-color)",
        backgroundColor: "var(--white)",
        borderRadius: "var(--border-radius-base)",
        transition: "background .95s var(--primary-ease)"
    }),
    singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = 'opacity 300ms';
        return { ...provided, opacity, transition, fontWeight: "bold" };
    },
}

const ReduxFormSelect = ({ input, options }) => {
    return (
        <Select
            {...input}
            isMulti
            styles={customStyles}
            onChange={value => input.onChange(value)}
            onBlur={() => input.onBlur(input.value)}
            options={options}
        />
    )
}


const selector = formValueSelector('UserForm');

class UserForm extends Component {

    getOptions = (options) => {
        return options.map(({ name, _id }) => ({ value: _id, label: name }))
    }

    render() {

        const { handleSubmit, pristine, submitting, hasAdminValue, vendors } = this.props;

        return (

            <form onSubmit={handleSubmit}>

                <Field
                    id="firstname"
                    label="First Name"
                    name="firstname"
                    type="text"
                    required
                    component={renderField}
                />

                <Field
                    id="lastname"
                    label="Last Name"
                    name="lastname"
                    type="text"
                    required
                    component={renderField}
                />

                <Field
                    id="username"
                    label="Username"
                    name="username"
                    type="text"
                    required
                    component={renderField}
                />

                <Field
                    id="email"
                    label="Email"
                    name="email"
                    type="email"
                    required
                    component={renderField}
                />

                <Field
                    id="phone"
                    label="Phone"
                    name="phone"
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    required
                    component={renderField}
                />

                <Field
                    id="isAdmin"
                    label="Admin"
                    name="admin"
                    type="checkbox"
                    component={renderCheckBox}
                />

                <Field
                    id="isActive"
                    label="Active"
                    name="is_active"
                    type="checkbox"
                    component={renderCheckBox}
                />


                <Divider />
                {!hasAdminValue &&
                    <>
                        <p><label>User can download list from the vendors:</label></p>
                        <Field
                            id="vendors"
                            label="Vendors"
                            name="vendors"
                            isMulti={true}
                            options={this.getOptions(vendors)}
                            component={ReduxFormSelect}
                        />
                        <Divider />
                    </>
                }

                <FormControl fullWidth>
                    <Button type="submit" disabled={submitting || pristine}>
                        Save
                    </Button>
                </FormControl>

            </form>
        )
    }

}

UserForm = reduxForm({ form: 'UserForm', validate, enableReinitialize: true})(UserForm)

UserForm = connect(state => {
    // can select values individually
    const hasAdminValue = selector(state, 'admin')
    return {
        hasAdminValue,
    }
})(UserForm)

export default UserForm