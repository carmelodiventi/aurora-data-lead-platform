const mongoose = require("mongoose");
const dbURL = process.env.NODE_ENV !== 'production' ? process.env.MONGODB_LOCALHOST : process.env.MONGODB_URL;

mongoose.connect(dbURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
}).then(() => {
    console.log('connected to MongoDB database');
}).catch(() => {
    console.log('failed connected to database');
});