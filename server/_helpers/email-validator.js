const dns = require('dns');
const validator = require('validator')

const validateContacts = async (items) => {
    let arr = [];
    for await (let item of items) {
        arr.push({
            ...item,
            validated: {
                ...item.validated,
                email: await validateEmailAddress(item.email || item.Email)
            }
        })
    }
    return arr;
}

const validateEmailAddress = async (email) => {

    if (!email || !validator.isEmail(email)) {
        return { isValid: false };
    }

    const domain = email.split('@')[1];

    return new Promise((resolve) => {
        dns.resolveMx(domain, (err, mx) => {
            if (typeof mx !== 'undefined') {
                mx ? resolve({ isValid: true, mx }) : resolve({ isValid: false })
            }
            else {
                resolve({ isValid: false })
                // reject(new Error(err));
            }
        });
    });

}

module.exports = {
    validateContacts
};