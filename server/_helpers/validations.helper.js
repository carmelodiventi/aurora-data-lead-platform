const { check } = require('express-validator');

exports.login = [ 
    check('email')
    .isEmail(),
    check('password')
    .notEmpty()
    .trim()
    .escape(),
    check('token')
    .notEmpty()
];

exports.forgotPassword = [ 
    check('email')
    .notEmpty()
    .isEmail()
];

exports.checkCode = [ 
    check('code')
    .notEmpty()
    .isLength({ min : 5})
    .isAlphanumeric()
];

exports.resetPassword = [ 
    check('email')
    .isEmail(),
    check('password').notEmpty()
];

exports.addUser = [ 
    check('email')
    .isEmail(),
    check('username')
    .isAlphanumeric()
    .isLength({min : 8})
];

exports.deleteUser = [ 
    check('id')
    .isMongoId()
];

exports.searchUser = [ 
    check('filterBy.email')
    .isEmail()
];

exports.searchOrder = [ 
    check('filterBy.number')
    .isNumeric()
];