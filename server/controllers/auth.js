const User = require('../models/User');
const fetch = require('node-fetch');
const Email = require('email-templates');

const login = async (req, res) => {
    try {
        const { email, password, g_token } = req.body;
        if (!email || !password) {
            throw new Error('Login failed! Check authentication credentials')
        }
       
        const user = await User.findByCredentials(email, password);
        if (!user) {
            throw new Error('Login failed! Check authentication credentials')
        }
        if (!user.is_active) {
            throw new Error('Login failed! Your Account has not been activated')
        }
        
        const secret_key = process.env.RECAPTCHA_KEY_SECRET;
        const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${g_token}`;
        const google_response = await fetch(url, { method: 'post' });
        const google_response_json = await google_response.json();
        if (!google_response_json.success) {
            throw new Error('Google Recaptcha error!')
        }

        const { authToken, refreshToken } = await user.generateAuthTokens();
        const userInfo = { email: user.email, role: user.role, is_active: user.is_active, firstname: user.firstname, lastname: user.lastname, username: user.username, phone : user.phone };
        res.cookie('token', authToken, { httpOnly: true });
        res.status(201).send({ success: true, user: userInfo, authToken, refreshToken, google_response_json });
    } catch (error) {
        res.status(400).send({ success: false, error: error.message })
    }
}

const logout = async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save()
        res.send();
    } catch (error) {
        res.status(500).send({ error: error.message })
    }
}

const logoutAll = async (req, res) => {
    try {
        req.user.tokens.splice(0, req.user.tokens.length)
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send({ error: error.message })
    }
}

const forgotPassword = async (req, res) => {
    try {
        const { email } = req.body;
        if (!email) {
            throw new Error('Email is required')
        }

        const user = await User.findByEmail(email);

        if (!user) {
            throw new Error('Login failed! Check authentication credentials')
        }

        const { username } = user;
        const code = await user.generateResetToken();

        const message = new Email({
            message: {
                from: `${process.env.APP_NAME} <${process.env.SMTP_USERNAME}>`, // sender address
            },
            preview: true,
            //send: true,
            transport: {
                host: process.env.SMTP_HOST,
                port: process.env.SMTP_PORT,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: process.env.SMTP_USERNAME,
                    pass: process.env.SMTP_PASSWORD
                }
            }
        });

        await message.send({
            template: 'reset_password',
            message: {
                to: email
            },
            locals: {
                username,
                email,
                code
            }
        }).then(() => {
            res.status(200).send({ success: true, message: `A code has been sent to ${email}`, email })
        })


    } catch (error) {
        res.status(500).send({ error: error.message })
    }
}

const checkCode = async (req, res) => {
    try {
        const { code, email } = req.body;
        if (!email) {
            throw new Error('Email is required!')
            // res.status(400).send({ error: 'Email is required' })
        }
        if (!code) {
            throw new Error('Code is required!')
            // res.status(400).send({ error: 'Code is required' })
        }
        const user = await User.findByCode(email, code);
        if (!user) {
            res.status(401).send({ error: 'Code! Check the code that has been sent!' })
        }

        res.status(200).send({ success: true, message: `The code is matching rightly, please provide to reset the password` })

    } catch (error) {
        res.status(500).send({ error: error.message })
    }
}

const resetPassword = async (req, res) => {
    try {
        const { email, password, code } = req.body;

        if (!email) {
            res.status(400).send({ error: 'Email is required' })
        }
        if (!code) {
            throw new Error('Code is required!')
            // res.status(400).send({ error: 'Code is required' })
        }

        const user = await User.findByCode(email, code);
        if (!user) {
            res.status(401).send({ error: 'Code! Check the code that has been sent!' })
        }

        await user.resetPassword(password);

        res.status(200).send({ success: true, message: `The password has been updated, please login as usual` })

    } catch (error) {
        res.status(500).send({ error: error.message })
    }
}

const refreshToken = async (req, res) => {
    try {
        const user = req.user;
        const { authToken, refreshToken } = await user.generateAuthTokens();
        res.cookie('token', authToken, { httpOnly: true });
        res.status(201).send({ authToken, refreshToken })
    } catch (error) {
        res.status(403).send({ success: false, error: error.message })
    }
}


module.exports = {
    login,
    logout,
    logoutAll,
    forgotPassword,
    checkCode,
    resetPassword,
    refreshToken
}