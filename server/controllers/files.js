var cloudinary = require('cloudinary').v2;


const upload = async (req, res) => {

    try {
        const { file } = req.body;
        await cloudinary.uploader.upload(file, function (error, result) {
            console.log(result, error)
        });
        res.status(200).send({});
    } catch (error) {
        res.status(400).send({ error: error.message })
    }

}

const _delete = async (req, res) => {

    try {
        const { id } = req.params;
        await cloudinary.uploader.destroy(id, function(error,result) {
            console.log(result, error) 
        });
        res.status(200).send({});

    } catch (error) {
        res.status(400).send({ error: error.message })
    }

}

module.exports = {
    upload,
    _delete
}