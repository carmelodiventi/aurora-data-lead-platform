const Vendor = require('../models/Vendors');
const cloudinary = require('cloudinary').v2;
const Busboy = require('busboy');


const getAll = async (req, res) => {
    try {
        const vendors = await Vendor.find({});
        res.status(200).json({ success: true, vendors });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const get = async (req, res) => {
    try {
        const { id } = req.params;
        const vendor = await Vendor.findOne({ _id: id });
        res.status(200).json({ success: true, vendor });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const add = async (req, res) => {
    try {
        const { name } = req.body;
        const vendorIsExisting = await Vendor.findOne({ name: name });
        if (vendorIsExisting) {
            throw new Error('A Vendor with that name has already registered. Please use a different Name..')
        }

        const vendor = new Vendor({ name });
        await vendor.save()

        res.status(200).json({ success: true, vendor });

    } catch (error) {
        res.status(400).send({ success: false, error: error.message })
    }
}

const update = async (req, res) => {
    try {
        const { id } = req.params;
        const { name } = req.body;
        const values = { name };
        if(req.files){
            const { tempFilePath } = req.files.logo;
            const { url } = await cloudinary.uploader.upload(tempFilePath);
            values.logo = url;
        }
        const vendor = await Vendor.findByIdAndUpdate({ _id: id }, {...values});
        res.status(200).json({ success: true, vendor });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const _delete = async (req, res) => {
    try {
        const { id } = req.params;
        const vendor = await Vendor.findOneAndDelete({ _id: id });
        res.status(200).json({ success: true, vendor });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}


module.exports = {
    getAll,
    get,
    add,
    update,
    _delete
}