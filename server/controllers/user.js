const User = require('../models/User');
const Email = require('email-templates');
const randtoken = require('rand-token');

const getAll = async (req, res) => {
    try {
        const users = await User.find({});
        res.status(200).json({ success: true, users });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const get = async (req, res) => {
    try {
        const { id } = req.params;
        const user = await User.findOne({ _id: id });
        res.status(200).json({ success: true, user });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const add = async (req, res) => {
    try {
        const { email, firstname, lastname, username, is_active, admin, vendors } = req.body;
        const password = randtoken.generate(8);
        const role = admin ? 'admin' : 'client';
        const userIsExisting = await User.findOne({ email: email });
        if (userIsExisting) {
            throw new Error('A user with that email has already registered. Please use a different email..')
        }
        const user = new User({ firstname, lastname, email, username, password, role, is_active, vendors});
        await user.save()
        
        const message = new Email({
            message: {
                from: `${process.env.APP_NAME} <${process.env.SMTP_USERNAME}>`, // sender address
            },
            preview: false,
            send: true,
            transport: {
                host: process.env.SMTP_HOST,
                port: process.env.SMTP_PORT,
                secure: false,
                auth: {
                    user: process.env.SMTP_USERNAME,
                    pass: process.env.SMTP_PASSWORD
                }
            }
        });

        await message.send({
            template: 'new_account',
            message: {
                to: email
            },
            locals: {
                username,
                email,
                password
            }
        }).then(() => {
            res.status(201).send({ success:true, message: "User registered successfully, an email with the password has been sent to the email", user })
        })


    } catch (error) {
        res.status(400).send({ success: false, error: error.message })
    }
} 

const update = async (req, res) => {
    try {
        const { id } = req.params;
        const { firstname, lastname, username, phone, admin, is_active, vendors } = req.body;
        const role = admin ? 'admin' : 'client';
        const belongTovendors = vendors !== "" ? vendors : [];
        const user = await User.findByIdAndUpdate(id, { firstname, lastname, username, phone, role, is_active, vendors: belongTovendors  });
        res.status(200).send({ success: true, message: "User updated successfully", user })
    } catch (error) {
        res.status(400).send({ success: false, error: error.message })
    }
}


const _delete = async (req, res) => {
    try {
        const { id } = req.params;
        console.log(id)
        const user = await User.findByIdAndDelete(id);
        res.status(200).json({ success: true, message: "User deleted successfully", user });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const updateProfile = async (req, res) => {
    try {
        const { _id } = req.user;
        const { firstname, lastname, username, phone } = req.body;
        const userInfo = await User.updateProfile(_id, { firstname, lastname, username, phone });
        res.status(200).send({ success: true, message: "Profile updated successfully", user : userInfo })
    } catch (error) {
        res.status(400).send({ success: false, error: error.message })
    }
}

const updateEmail = async (req, res) => {
    try {
        const { _id } = req.user;
        const { email } = req.body;
        const userInfo = await User.updateEmail(_id, { email });
        res.status(200).send({ success: true,  message: "Email updates successfully", user : userInfo })
    } catch (error) {
        res.status(400).send({ success: false, error: error.message })
    }
}

const updatePassword = async (req, res) => {
    try {
        const { _id, email } = req.user;
        const { oldPassword , password  } = req.body;
        await User.findByCredentials(email, oldPassword);
        const userInfo = await User.updatePassword(_id, { password });
        res.status(200).send({ success: true, message: "Password updates successfully", user : userInfo })
    } catch (error) {
     
        res.status(400).send({ success: false, error: error.message })
    }
}

const me = async (req, res) => {
    try {
        res.send(req.user)
    } catch (error) {
        res.status(500).send({ error: error.message })
    }
}


module.exports = {
    getAll,
    get,
    add,
    _delete,
    update,
    me,
    updateProfile,
    updateEmail,
    updatePassword,
}