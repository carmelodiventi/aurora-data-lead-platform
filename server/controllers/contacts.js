const axios = require('axios');
const Contacts = require('../models/Contacts');
const Logs = require('../models/Logs');

const getAll = async (req, res, next) => {
    try {
        const { offset, limit } = req.query;
        const options = {
            offset,
            limit
        };
        const contacts = await Contacts.paginate({}, options);
        res.status(200).json({ success: true, contacts });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const getByID = async (req, res, next) => {
    try {
        
    } catch (error) {
        
    }
}

const add = async (req, res, next) => {
    try {
        const params = req.body;
        const contact = await Contact.findOne({ email: params.email });

        if (contact) {
            console.log(`Contact with email: ${params.email} has been replaced`);
            Object.assign(contact, params);
            await contact.save();
        }
        // make a new const calling the List model 

        if (!params.email) {
            throw 'Invalid Inputs';
        }

        const newContact = new Contacts(contactParam);

        await newContact.save();

        res.status(200).send({ contact: newContact });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const addBulk = async (req, res) => {
    try {
        const { items } = req.body;
        if (!items.length === 0) {
            throw 'Invalid Inputs';
        }
        const contacts = await Contacts.insertMany(items);
        res.status(200).send({ contacts: contacts, success: true })
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}


const _import = async (req, res) => {
    try {
        console.log(req.body)
        //const contact = new Contacts(...req.body);
        //await contact.save();
        res.status(200).send({ success: true });
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}


const _export = async (req, res) => {
    try {

        const { toURL, contacts } = req.body;
        const { id } = req.user;

        let axiosArray = [];

        contacts.forEach(contact => {
            let newPromise = axios.post(toURL, null, { params: { ...contact } });
            axiosArray.push(newPromise);
        });

        const requests = await axios.all(axiosArray);
    
        const details = requests.map( item => ({
            status: item.status,
            statusText: item.statusText,
            data : item.data
        }))

        const logs =  new Logs({ 
            user: id, 
            operation: `Exporting Contacts to URL ${toURL}`,
            details
        })

        await logs.save();

        res.status(200).send({ success : true, message: 'The List has been exported' });

    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const update = async (req, res) => {
    try {
        const { id } = req.params;
        const { items } = req.body;
        if (!id) {
            throw 'ID is missing';
        }
        const contact = await Contacts.findByIdAndUpdate(id, items);
        res.status(200).send({ contact, success: true })
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const _delete = async (req, res) => {
    try {
        const { id } = req.params;
        if (!id) {
            throw 'ID is missing';
        }
        const contact = await Contacts.findByIdAndDelete(id);
        res.status(200).send({ contact, success: true })
    } catch (error) {
        res.status(400).send({ error: error.message })
    }
}

module.exports = {
    getAll,
    getByID,
    add,
    addBulk,
    update,
    _import,
    _export,
    _delete
}