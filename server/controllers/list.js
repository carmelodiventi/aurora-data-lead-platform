const Lists = require('../models/Lists');
const Contacts = require('../models/Contacts');
const { validateContacts } = require('../_helpers/email-validator');

const getAll = async (req, res) => {
    try {
        const { offset, limit, name, startDate, endDate } = req.query;
        const user = req.user;
        const options = { populate: ['template', 'vendor'], offset, limit, sort: { created_at: -1 } };
        let query = {};
        if(name){
            query = {...query, name: new RegExp(name, 'i') };
        }
        if (startDate && endDate){
            query = {...query, created_at: { $gte: startDate, $lte: endDate } };
        }
        switch (user.role) {
            case 'client':
                query = { ...query, vendor: user.vendors };
        }
        const lists = await Lists.paginate(query, options);
        res.status(200).json({ success: true, lists });
    } catch (error) {
        res.status(400).send({ error: error.message })
    }

}

const getById = async (req, res) => {
    try {
        const { id } = req.params;
        const { filters } = req.query;
        let contacts;

        if (filters) {
            let filtersItem = JSON.parse(filters);
            // Mapping the obj with RegExp with "i" to ignore the case 
            Object.entries(filtersItem).forEach(([key, val]) => {
                filtersItem[key] = new RegExp(val, 'i')
            });

            contacts = await Contacts.find({ list: id, ...filtersItem });
        }
        else {
            contacts = await Contacts.find({ list: id });
        }

        const list = await Lists.findOne({ _id: id }).populate(['template', 'vendor']);

        res.status(200).json({ contacts, list });

    } catch (error) {
        res.status(400).send({ error: error.message })
    }

}

const add = async (req, res) => {

    try {
        const { items, name, template, vendor } = req.body;
        if (!items.length || name === '' || template === '') {
            throw new Error('Contacts items are empty or the list name is incorrect');
        }
        const listname = name.replace(/\s+/g, '_');
        const list = new Lists({ name: listname, template, vendor, created_at: Date.now() });
        const itemsInList = items.map(item => item = { ...item, list: list.id });
        const validatedContacts = await validateContacts(itemsInList);
        const contacts = await Contacts.insertMany(validatedContacts);
        await list.save();

        res.status(200).send({ contacts: contacts, success: true })
    } catch (error) {
        res.status(400).send({ error: error.message })
    }

}

const _delete = async (req, res) => {

    try {
        const { id } = req.params;
        if (!id) {
            throw new Error('The list ID is not defined')
        }
        const list = await Lists.findByIdAndDelete(id);
        const contacts = await Contacts.deleteMany({ list: id });
        res.status(200).send({ list, success: true });
    } catch (error) {
        res.status(400).send({ error: error.message })
    }

}

module.exports = {
    getAll,
    getById,
    add,
    _delete
}