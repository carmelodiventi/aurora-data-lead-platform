const Logs = require("../models/Logs");

const getAll = async (req, res) => {

    const { offset, limit } = req.query;
    const options = { populate: ['user'], offset, limit };
    const logs = await Logs.paginate({}, options);
    res.status(200).json({ success: true, logs });

}

module.exports = {
    getAll
}