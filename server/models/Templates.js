const mongoose = require('mongoose');

const templatesSchema = mongoose.Schema({
    name : String,
    fields : Object
})

const FieldsTemplate = mongoose.model('FieldsTemplate', templatesSchema);

module.exports = FieldsTemplate;