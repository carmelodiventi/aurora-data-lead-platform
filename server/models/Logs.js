const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const logsSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    operation: {
        type: String,
        required: true
    },
    details: {
        type: Object,
        required: true,
    },
    created_at: { type: Date, default: Date.now() },
})

logsSchema.plugin(mongoosePaginate);
const Logs = mongoose.model('Logs', logsSchema);

module.exports = Logs;