
//Require Mongoose
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const ListModelSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  template: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'FieldsTemplate'
  },
  vendor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendors'
  },
  created_at: { 
    type: Date, 
    default: Date.now(),
    required: true
  },
});
 
ListModelSchema.plugin(mongoosePaginate);
ListModelSchema.set('autoIndex', true);

const Lists = mongoose.model('List', ListModelSchema );

module.exports = Lists;