
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;

const ContactsModelSchema = new Schema({
  list: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'list'
  }
}, { strict: false });

ContactsModelSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Contacts', ContactsModelSchema);