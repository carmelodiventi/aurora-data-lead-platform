const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const randtoken = require('rand-token').generator();

const UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true,
        trim: true
    },
    firstname: {
        type: String,

        trim: true
    },
    lastname: {
        type: String,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true,
        validate: value => {
            if (!validator.isEmail(value)) {
                throw new Error({ error: 'Invalid Email address' })
            }
        }
    },
    password: {
        type: String,
        required: true,
        minLength: 8
    },
    role: {
        type: String,
        required: true
    },
    is_active: {
        type: Boolean,
        required: true,
        default: function () {
            return false
        }
    },
    vendors: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Vendors',
        default: undefined
    }],
    reset_password_token: {
        type: String,
        minLength: 24,
        default: function () {
            return randtoken.generate(24)
        }
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    createdDate: { type: Date, default: Date.now }
})

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) {
        return next();
    }
    // Hash the password before saving the user model
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})

UserSchema.pre('findByIdAndUpdate', async function (next) {
    // Hash the password before saving the user model
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})

UserSchema.methods.generateAuthTokens = async function () {
    // Generate an auth token for the user
    const user = this;
    // Users tokens
    const authToken = jwt.sign({ _id: user._id, role: user.role }, process.env.JWT_KEY, { expiresIn: '1h' });
    const refreshToken = jwt.sign({ _id: user._id }, process.env.JWT_KEY, { expiresIn: '1w' });
    user.tokens = user.tokens.concat({ token: authToken });
    user.refresh_token = refreshToken;
    //
    await user.save();
    const tokens = { authToken, refreshToken };
    return tokens;
}


UserSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.

    const user = await User.findOne({ email });

    if (!user) {
        throw new Error('User not found')
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (!isPasswordMatch) {
        throw new Error('Password does not match')
    }
    return user
}

UserSchema.statics.findByEmail = async (email) => {
    // Search for a user by email.
    const user = await User.findOne({ email });
    if (!user) {
        throw new Error('User not found')
    }
    return user
}

UserSchema.statics.checkEmail = async (email) => {
    // Search for a user by email.
    return await User.findOne({ email });
}


UserSchema.statics.findByCode = async (email, reset_password_token) => {
    // Search for a user by email and code.
    const user = await User.findOne({ email, reset_password_token });
    if (!user) {
        throw new Error('User not found with the code Provided')
    }
    return user
}

UserSchema.statics.updateProfile = async function (_id, { firstname, lastname, phone }) {
    const user = this;
    return await user.findByIdAndUpdate(_id, { firstname, lastname, phone }, { new: true });
}

UserSchema.statics.updateEmail = async function (_id, { email }) {
    const user = this;
    const userEmailTaken = await user.checkEmail(email);
    if (userEmailTaken) {
        throw new Error('An error are occured, User email has been taken!')
    }
    return await user.findByIdAndUpdate(_id, { email }, { new: true });
}

UserSchema.statics.updatePassword = async function (_id, { password }) {
    const user = this;
    const hashpassword = await bcrypt.hash(password, 8)
    return await user.findByIdAndUpdate(_id, { password: hashpassword });
}

UserSchema.methods.resetPassword = async function (password) {
    const user = this;
    user.password = password;
    await user.save();
    return user;
}

UserSchema.methods.generateResetToken = async function () {
    const user = this;
    const token = randtoken.generate(24);
    user.reset_password_token = token;
    await user.save();
    return token;
}


const User = mongoose.model('User', UserSchema);

module.exports = User;