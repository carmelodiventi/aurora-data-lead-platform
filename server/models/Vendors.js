const mongoose = require('mongoose');

const vendorsSchema = mongoose.Schema({
    name : String,
    logo: String,
    mediaAgencies: Object
})

const Vendors = mongoose.model('Vendors', vendorsSchema);

module.exports = Vendors;