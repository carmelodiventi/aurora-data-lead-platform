const jwt = require('jsonwebtoken')
const User = require('../models/User')

const auth = async (req, res, next) => {
    const authorization = req.header('Authorization')
    if (authorization) {
        try {
            const token = authorization.replace('Bearer ', '');
            const data = await jwt.verify(token, process.env.JWT_KEY, (err, data) => {
                if (err) {
                    throw new Error(err)
                }
                return data;
            });
            const user = await User.findOne({ _id: data._id, 'tokens.token': token })
            if (!user) {
                throw new Error('User not found with this credentials')
            }
            req.user = user
            req.token = token
            next()
        } catch (error) {
            res.status(401).send({ error: error.message })
        }
    }
    else {
        res.status(403).send({ error: 'Not token provided' })
    }
}

const admin = async (req, res, next) => {
    const authorization = req.header('Authorization')
    if (authorization) {
        try {
            const token = authorization.replace('Bearer ', '');
            const data = await jwt.verify(token, process.env.JWT_KEY);
            const user = await User.findOne({ _id: data._id, 'tokens.token': token })
            if (!user) {
                throw new Error('User not found with this credentials')
            }
            const { role } = user;
            if (role !== 'admin') {
                throw new Error('Can\'t perform this action due to your role')
            }
            req.user = user
            req.token = token
            next()
        } catch (error) {
            res.status(401).send({ error: error.message })
        }
    }
    else {
        res.status(403).send({ error: 'Not token provided' })
    }
}

const checkToken = async (req, res, next) => {
    const authorization = req.header('Authorization')
    if (authorization) {

        try {

            const { token } = req.body;

            const data = await jwt.verify(token, process.env.JWT_KEY, (err, data) => {
                if (err) {
                    throw new Error(err)
                }
                return data;
            });

            const user = await User.findOne({ _id: data._id });
            if (!user) {
                throw new Error('User not found with this credentials')
            }
            req.user = user;
            next();

        } catch (error) {
            res.status(401).send({ error: error.message })
        }
    }
    else {
        res.status(403).send({ error: 'No token provided' })
    }

}


module.exports = {
    auth,
    admin,
    checkToken
}