const router = require("express").Router();
const logs = require("../../controllers/logs");
const { admin } = require('../../middleware/auth');

router.route('/').get(admin, logs.getAll);

module.exports = router;