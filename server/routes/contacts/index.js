const express = require('express')
const contacts = require('../../controllers/contacts')
const router = express.Router();
const { auth, admin } = require('../../middleware/auth');

router.route('/').get(auth, contacts.getAll)
router.route('/').post(admin, contacts.add)
router.route('/import').post(admin, contacts._import)
router.route('/insert/bulk').post(admin, contacts.addBulk)
router.route('/:id').get(auth, contacts.getByID)
router.route('/:id').delete(admin, contacts._delete)
router.route('/:id').put(admin, contacts.update)
router.route('/export').post(auth, contacts._export)


module.exports = router;