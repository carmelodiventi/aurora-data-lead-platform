const express = require('express')
const user = require('../../controllers/user')
const router = express.Router();
const { auth, admin } = require('../../middleware/auth');

router.route('/').get(admin, user.getAll)
router.route('/:id').get(admin, user.get)
router.route('/').post(user.add)
router.route('/:id').patch(auth, user.update)
router.route('/:id').delete(admin, user._delete)
router.route('/me').get(auth, user.me)
router.route('/me/update-profile').patch(auth, user.updateProfile)
router.route('/me/update-password').patch(auth, user.updatePassword)
router.route('/me/update-email').patch(auth, user.updateEmail)

module.exports = router;