const express = require('express')
const router = express.Router();
const files = require('../../controllers/files')
const {auth} = require('../../middleware/auth');

router.route('/').post(auth,files.upload)
router.route('/:id').delete(auth,files._delete)

module.exports = router;