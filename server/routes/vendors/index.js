const express = require('express');
const vendors = require('../../controllers/vendors');
const router = express.Router();
const { admin } = require('../../middleware/auth');

router.route('/').get(admin, vendors.getAll)
router.route('/id/:id').get(admin, vendors.get)
router.route('/').post(admin, vendors.add)
router.route('/:id').post(admin, vendors.update)
router.route('/:id').delete(admin, vendors._delete)

module.exports = router;